<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">Carousal Slide Setting</div>

<div class="panel-body" id="demo_s">
		<table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" >
			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('image');?></th>
                    <th><?php echo translate('button');?></th>
                    <th><?php echo translate('status');?></th>
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
				
			<tbody >
			<?php
				$i=0;
            	foreach($all_slidess as $row){
            		$i++;
			?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td >
                        <img class="img-md"
                            src="<?php echo $this->crud_model->file_view('slides',$row['slides_id'],'100','','thumb','src','','','.jpg') ?>"  style="width:120px;"/>
                    </td>
                    <td>
                    	<?php if($row['button_text']!=NULL){ ?>
                    	<a class="btn btn-xs" style="background:<?php echo $row['button_color']; ?>; color:<?php echo $row['text_color']; ?>" href="<?php echo $row['button_link']; ?>"
                        	data-toggle="tooltip" title="<?php echo translate('click_to_check_link');?>">
							<?php echo $row['button_text']; ?>
                        </a>
                        <?php } ?>
                    </td>
                    <td>
                    	<input id="slide_<?php echo $row['slides_id']; ?>" class="slide" type="checkbox" data-id="<?php echo $row['slides_id']; ?>" <?php if($row['status']=='ok'){ echo 'checked'; } ?> />
                    </td>
                    <td class="text-right">
                        <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['slides_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                        <span class="sidebar-icon ti-pencil pull-right"    onclick="ajax_modal('edit','<?php echo translate('edit_slides'); ?>','<?php echo translate('successfully_edited!'); ?>','slides_edit','<?php echo $row['slides_id']; ?>')"
                              style="font-size:18px;margin-top:10px;color:#00acac !important"></span>
                        
                    </td>
                </tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>
        </div>
    </div>
           
	<div id='export-div'>
		<h1 style="display:none;"><?php echo translate('slides'); ?></h1>
		<table id="export-table" data-name='slides' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('name');?></th>
						<th><?php echo translate('category');?></th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					$i = 0;
	            	foreach($all_slidess as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row['name']; ?></td>
					<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></td>
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>

<style>
	.highlight{
		background-color: #E7F4FA;
	}
</style>

<script>
var base_url = '<?php echo base_url(); ?>'
var user_type = 'vendor';
var module = 'slides';
function set_switchery(){
	$(".slide").each(function(){
		new Switchery(document.getElementById('slide_'+$(this).data('id')), {color:'rgb(100, 189, 99)', secondaryColor: '#cc2424', jackSecondaryColor: '#c8ff77'});
		var changeCheckbox = document.querySelector('#slide_'+$(this).data('id'));
		changeCheckbox.onchange = function() {
		  //alert($(this).data('id'));
		  ajax_load(base_url+'index.php/'+user_type+'/'+module+'/slide_publish_set/'+$(this).data('id')+'/'+changeCheckbox.checked,'','');
		  if(changeCheckbox.checked == true){
			$.activeitNoty({
				type: 'success',
				icon : 'fa fa-check',
				message : ppus,
				container : 'floating',
				timer : 3000
			});
			sound('published');
		  } else {
			$.activeitNoty({
				type: 'danger',
				icon : 'fa fa-check',
				message : pups,
				container : 'floating',
				timer : 3000
			});
			sound('unpublished');
		  }
		  //alert(changeCheckbox.checked);
		};
	});
}
	
</script>