<div id="content" class="content">
    <div class="" style="">

        <div class="" >


            <a style="margin-right:3px"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right"
               onclick="ajax_modal('add','<?php echo translate('add_slides'); ?>','<?php echo translate('successfully_added!');?>','slides_add','')">
                <span class="sidebar-icon ti-plus"></span> <?php echo translate('create_slides');?>
            </a>

        </div>
        <h1 class="page-header"><?php echo translate('manage_public_profile_slider');?></h1>
    </div>
    <div class="row" id="list">

    </div>
</div>


<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'vendor';
	var module = 'slides';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>

