
<!-- begin #top-menu -->
<div id="top-menu" class="top-menu">
    <!-- begin top-menu nav -->
    <ul class="nav" style="margin-left:0px !important;">
        <li <?php if($page_name=="dashboard"){?> class="active" <?php } ?>>
            <a href="<?php echo base_url(); ?>index.php/vendor/">
                <span class="sidebar-icon ti-home "></span>
                <span><?php echo translate('home');?></span>
            </a>

        </li>
        <?php
        if($physical_check == 'ok' && $digital_check == 'ok') {
            if ($this->crud_model->vendor_permission('product') ||
                $this->crud_model->vendor_permission('stock') ||
                $this->crud_model->vendor_permission('digital')
            ) {
                ?>
                <li <?php if($page_name=="product" ||
                    $page_name=="stock" ||
                    $page_name=="digital" ){?>
                    class="has-sub active"
                <?php } else { ?> class="has-sub" <?php } ?>>
                    <a href="javascript:">
                        <b class="caret pull-right"></b>
                        <span class="sidebar-icon ti-briefcase"></span>
                        <span><?php echo translate('products'); ?></span>
                    </a>
                    <ul class="sub-menu">
                        <?php
                        if($this->crud_model->vendor_permission('product') ||
                            $this->crud_model->vendor_permission('stock') ) {
                            ?>
                            <li class="has-sub">
                                <a href="javascript:;"><b class="caret pull-right"></b> <?=translate('physical_products');?></a>
                                <ul class="sub-menu">
                                    <?php  if($this->crud_model->vendor_permission('product')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/vendor/product"> <?php echo translate('all_products');?></a></li>
                                    <?php } if($this->crud_model->vendor_permission('stock')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/vendor/stock"><?php echo translate('product_stock');?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>


                            <?php
                        }

                        if($this->crud_model->vendor_permission('digital') ){
                            ?>

                            <li>
                                <a href="<?php echo base_url(); ?>index.php/vendor/digital"><?php echo translate('digital_products');?></a>

                            </li>


                            <?php
                        }


                        ?>

                    </ul>
                </li>


                <?php
            }
        }
        ?>
        <?php
        if($this->crud_model->vendor_permission('sale')){
            ?>
            <li <?php if($page_name=="sales"){?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/vendor/sales/">
                    <span class="fa fa-shopping-cart"></span>
                    <span><?php echo translate('sale');?></span>
                </a>

            </li>
            <?php
        }
        if($this->crud_model->vendor_permission('pay_to_vendor'))
        {
            ?>
            <li <?php if($page_name=="admin_payments"){?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/vendor/admin_payments/">
                    <span class="sidebar-icon ti-wallet"></span>
                    <span><?php echo translate('payments');?></span>
                </a>

            </li>
            <?php
        }

        if($this->crud_model->vendor_permission('business_settings')){
            ?>
            <li <?php if($page_name=="upgrade_history" || $page_name=="package"){?> class="active has-sub" <?php } else { ?> class="has-sub" <?php } ?>>
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <i class="fa fa-subscript"></i>
                    <span><?php echo translate('vendor_packages');?></span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/upgrade_history/"><?php echo translate('package_upgrade_history');?></a> </li>
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/package/"> <?php echo translate('upgrade_package');?></a></li>
                </ul>

            </li>
            <?php
        }

        if($this->crud_model->vendor_permission('coupon'))
        {
            ?>
            <li <?php if($page_name=="coupon"){?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/vendor/coupon/">
                    <span class="sidebar-icon ti-gift"></span>
                    <span><?php echo translate('discount_coupon');?></span>
                </a>

            </li>
            <?php
        }
        if($this->crud_model->vendor_permission('report')){
            ?>
            <li <?php if($page_name=="report" ||
                $page_name=="report_stock" ||
                $page_name=="report_wish" ){?> class="has-sub active" <?php } else {?> class="has-sub" <?php }?>>
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <span class="sidebar-icon ti-stats-up"></span>
                    <span><?php echo translate('reports');?></span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/report/">  <?php echo translate('product_compare');?></a></li>
                    <?php if($physical_check=='ok') {?>
                        <li><a href="<?php echo base_url(); ?>index.php/vendor/report_stock/"> <?php echo translate('product_stock');?></a></li>
                    <?php }?>
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/report_wish/"> <?php echo translate('product_wishes');?></a></li>

                </ul>

            </li>

            <?php
        }

        if($this->crud_model->vendor_permission('slides') ||
            $this->crud_model->vendor_permission('site_settings') ||
            $this->crud_model->vendor_permission('business_settings')){
            ?>
            <li <?php if($page_name=="business_settings" || $page_name=="slides" || $page_name=='site_settings'){?> class="active has-sub" <?php } else { ?> class="has-sub" <?php } ?>>
                <a href="javascript:;">
                    <b class="caret pull-right"></b>
                    <span class="sidebar-icon ti-settings"></span>
                    <span><?php echo translate('settings');?></span>
                </a>
                <ul class="sub-menu">
                    <?php if($this->crud_model->vendor_permission('slides')) { ?>
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/slides/"><?php echo translate('slider_settings');?></a> </li>
                    <?php } if($this->crud_model->vendor_permission('site_settings')) { ?>
                    <li><a href="<?php echo base_url(); ?>index.php/vendor/site_settings/general_settings/"> <?php echo translate('site_settings');?></a></li>
                    <?php } if($this->crud_model->vendor_permission('business_settings')) { ?>
                        <li><a href="<?php echo base_url(); ?>index.php/vendor/business_settings/"> <?php echo translate('payment_settings');?></a></li>
                    <?php }  ?>
                </ul>

            </li>
            <?php
        }
        if($this->crud_model->vendor_permission('manage_vendor'))
        {
            ?>
            <li <?php if($page_name=="manage_vendor"){?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/vendor/manage_vendor/">
                    <span class="sidebar-icon ti-user"></span>
                    <span><?php echo translate('profile');?></span>
                </a>

            </li>
            <?php
        }
        ?>




        <!-- <li class="menu-control menu-control-left">
             <a href="#" data-click="prev-menu"><i class="fa fa-angle-left"></i></a>
         </li>
         <li class="menu-control menu-control-right">
             <a href="#" data-click="next-menu"><i class="fa fa-angle-right"></i></a>
         </li>-->
    </ul>
    <!-- end top-menu nav -->
</div>
