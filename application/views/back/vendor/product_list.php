<script src="<?php echo base_url(); ?>template/back/plugins/bootstrap-table/extensions/export/bootstrap-table-export.js"></script>
<div class="panel-body" id="demo_s">
    <table id="data-table" class="table table-bordered table-striped" data-side-pagination="server" data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]"  data-show-refresh="true" data-search="true"  data-show-export="true" >
        <thead>
            <tr>
                <th data-field="image" data-align="right" data-sortable="true">
                    <?php echo translate('image');?>
                </th>
                <th data-field="title" data-align="center" data-sortable="true">
                    <?php echo translate('title');?>
                </th>
                <th data-field="current_stock" data-sortable="true">
                    <?php echo translate('current_quantity');?>
                </th>
                <th data-field="publish" data-sortable="false">
                    <?php echo translate('publish');?>
                </th>
                <th data-field="options" data-sortable="false" data-align="right">
                    <?php echo translate('options');?>
                </th>
            </tr>
        </thead>

        <tbody>

        <?php
        $i = 0;
        foreach ($all_products as $row) {
            $i++;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><img class="img-sm" style="height:auto !important; border:1px solid #ddd;padding:2px; border-radius:2px !important;" src="<?php echo $this->crud_model->file_view('product',$row['product_id'],'','','thumb','src','multi','one');?>"> </td>
                <td><?php echo $row['title']; ?></td>
                <?php
                if($row['current_stock'] > 0){ ?>
                    <td><?php echo $row['current_stock'].$row['unit'].'(s)';?></td>
                <?php } else { ?>
                    <td><span class="label label-danger"><?=translate('out_of_stock');?></span></td>
                <?php }
                ?>

                <?php
                if($row['status'] == 'ok'){ ?>
                    <td><input id="pub_<?php echo $row['product_id'];?>" class="sw1" type="checkbox" data-id="<?php echo $row['product_id'];?>" checked /></td>
                <?php } else { ?>
                    <td><input id="pub_<?php echo $row['product_id'];?>" class="sw1" type="checkbox" data-id="<?php echo $row['product_id'];?>" /></td>
                <?php }
                ?>

                <td class="text-right">

 <span class="sidebar-icon ti-eye"
       onclick="ajax_set_full('view','<?php echo translate('view_product');?>','<?php echo translate('successfully_viewed!');?>','product_view','<?php echo $row['product_id'];?>');proceed('to_list');"
       style="font-size:18px;margin-top:10px;color:#34b2e7 !important" title="<?php echo translate('view');?>"></span>
                    <span class="sidebar-icon ti-gift"
                          onclick="ajax_modal('add_discount','<?php echo translate('view_discount');?>','<?php echo translate('viewing_discount!');?>','add_discount','<?php echo $row['product_id'];?>');"
                          style="font-size:18px;margin-top:10px;color:#8f60b6 !important" title="<?php echo translate('discount');?>"></span>
                    <span class="sidebar-icon ti-plus"
                          onclick="ajax_modal('add_stock','<?php echo translate('add_product_quantity');?>','<?php echo translate('quantity_added!');?>','stock_add','<?php echo $row['product_id'];?>');"
                          style="font-size:18px;margin-top:10px;color:#18af92 !important" title="<?php echo translate('add_product_quantity');?>"></span>
                    <span class="sidebar-icon ti-minus"
                          onclick="ajax_modal('destroy_stock','<?php echo translate('reduce_product_quantity');?>','<?php echo translate('quantity_reduced!');?>','destroy_add','<?php echo $row['product_id'];?>');"
                          style="font-size:18px;margin-top:10px;color:#383a42 !important" title="<?php echo translate('reduce_product_quantity');?>"></span>

                    <span class="sidebar-icon ti-pencil"
                          onclick="ajax_set_full('edit','<?php echo translate('edit_product');?>','<?php echo translate('successfully_edited!');?>','product_edit','<?php echo $row['product_id'];?>');proceed('to_list');"
                          style="font-size:18px;margin-top:10px;color:#00a65a !important" title="<?php echo translate('edit');?>"></span>
                    <span class="sidebar-icon ti-trash" onclick="delete_confirm('<?php echo $row['product_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important" title="<?php echo translate('delete');?>">


            </span>



                </td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#data-table').DataTable();
    });

</script>

