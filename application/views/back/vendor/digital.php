<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >

            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right add_pro_btn"
               onclick="ajax_set_full('add','<?php echo translate('add_product'); ?>','<?php echo translate('successfully_added!'); ?>','digital_add',''); proceed('to_list');">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_product');?>
            </a>
            <a style="margin-right:3px;display: none;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_set_list();  proceed('to_add');">
                <span class="sidebar-icon ti-arrow-left"></span> <?php echo translate('back_to_product_list');?>
            </a>

        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_product_(_digital_)');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>
</div>
<span id="prod" style="display:none;"></span>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var timer = '<?php $this->benchmark->mark_time(); ?>';
	var user_type = 'vendor';
	var module = 'digital';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
	
	function proceed(type){
		if(type == 'to_list'){
			$(".pro_list_btn").show();
			$(".add_pro_btn").hide();
		} else if(type == 'to_add'){
			$(".add_pro_btn").show();
			$(".pro_list_btn").hide();
		}
	}
	
	function digital_download(id){
		$.ajax({
                url: base_url+'index.php/'+user_type+'/'+module+'/can_download/'+id, // form action url
                type: 'GET', // form submit method get/post
                dataType: 'html', // request type html/json/xml
                beforeSend: function() {
					
                },
                success: function(data) {
					if(data == 'yes'){
						window.location = base_url+'index.php/'+user_type+'/'+module+'/download_file/'+id;	
					}else if(data == 'no'){
						$.activeitNoty({
							type: 'danger',
							icon : 'fa fa-times',
							message : '<?php echo translate('download_failed!'); ?>',
							container : 'floating',
							timer : 3000
						});
					}
                },
                error: function(e) {
                    console.log(e)
                }
            });
		 
	}
</script>

