<div id="content" class="content">
	<div id="page-title">
		<h1 class="page-header text-overflow" >
			<?php echo translate('manage_package_upgrade_history');?>
        </h1>
	</div>

                <!-- LIST -->
                <div class="row" id="list">
                
                </div>

</div>

<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'vendor';
	var module = 'upgrade_history';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>

