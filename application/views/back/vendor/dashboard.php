<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:02 PM
 */
?>
<div id="content" class="content">
	<!--<div class="row final-succeed"> <div class="col-md-12 col-sm-12"><span class="ico"></span>
    <h2>Ready for ERP Revolution</h2>
  </div> </div>-->
<div class="row final-site-nav">
   <div class="arrow"></div>
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="pms">
        <div class="ico"></div>
        <h5><a href="">Customers</a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="crm">
        <div class="ico"></div>
        <h5><a href="">Products</a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="hr">
        <div class="ico"></div>
        <h5><a href="">Vendors</a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="billing">
        <div class="ico"></div>
        <h5><a href="">Sales</a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="survey">
        <div class="ico"></div>
        <h5><a href="">Campaigns & Surveys </a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
				<!-- begin col-3 -->
				<div class="col-md-2 col-sm-6">
					 <div class="reports">
        <div class="ico"></div>
        <h5><a href="">Reporting</a></h5>
     
      </div>
				</div>
				<!-- end col-3 -->
			</div>
	
			
			<div class="row">
			<div class="col-md-8">
			<h5><?=translate('dashboard');?></h5><hr/>
			<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
					
						<div class="stats-info">
							<h4>Total Outstanding Invoices</h4>
							<p>Kshs 3,291,922</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
				
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
					
						<div class="stats-info">
							<h4>Overdue Invoices</h4>
							<p>Kshs 278,990</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
				
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
					
						<div class="stats-info">
							<h4>Revenue This Month</h4>
							<p>Kshs 5,901,874</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
					
						<div class="stats-info">
							<h4>Revenue This Year</h4>
							<p>Kshs 45,901,874</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
						
						<div class="stats-info">
							<h4>Revenue per client</h4>
							<p>Kshs 50,545</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
				
			
				
				<!-- begin col-4 -->
				<div class="col-md-4 col-sm-6">
					<div class="widget widget-stats">
						<div class="stats-icon"><i class="fa fa-desktop"></i></div>
						<div class="stats-info">
							<h4>Total Customers</h4>
							<p>56</p>	
						</div>
					
					</div>
				</div>
				<!-- end col-4 -->
			</div>
			
			<div class="col-md-4">
			<h5> Latest Sales</h5><hr/>
			<div id="dashboard-activity-list" class="unfloat-on-mobile">
   
            <div class="activity-stream">
        <ul class="activity-stream-list">
            
            <li class="activity-invoice activity-updated" data-id="13">
                <a href="#users/1" class="user-image"><img src=""></a>

                <div class="activity-content">

                    <a href="#users/1" class="activity-user">Sample Admin</a>
updated an
                    invoice




                    <span class="activity-timestamp" title="Mon Jan 23 11:13 am">a month ago</span>

                    <div class="activity-title">
                        
                        <a href="#projects/1/invoices/1" class="object-link">#201000</a>
                        <span class="object-link deleted-object">#201000</span>
                    </div>
                </div>
            </li>
            
            <li class="activity-invoice activity-updated" data-id="3">
                <a href="#users/1" class="user-image"><img src=""></a>

                <div class="activity-content">

                    <a href="#users/1" class="activity-user">Sample Admin</a>
updated an
                    invoice




                    <span class="activity-timestamp" title="Fri Jan 13 4:42 pm">2 months ago</span>

                    <div class="activity-title">
                        
                        <a href="#projects/1/invoices/1" class="object-link">#201000</a>
                        <span class="object-link deleted-object">#201000</span>
                    </div>
                </div>
            </li>
            
            <li class="activity-invoice activity-created" data-id="2">
                <a href="#users/1" class="user-image"><img src=""></a>

                <div class="activity-content">

                    <a href="#users/1" class="activity-user">Sample Admin</a>
created an
                    invoice




                    <span class="activity-timestamp" title="Fri Jan 13 4:40 pm">2 months ago</span>

                    <div class="activity-title">
                        
                        <a href="#projects/1/invoices/1" class="object-link">#201000</a>
                        <span class="object-link deleted-object">#201000</span>
                    </div>
                </div>
            </li>
            
            <li class="activity-project activity-Status change on" data-id="1">
                <a href="#" class="user-image"><img src="http://localhost/duet/server/files-folder/user_images/system.jpg"></a>

                <div class="activity-content">

Status change on a
                    project




                    <span class="activity-timestamp" title="Fri Jan 13 4:40 pm">2 months ago</span>

                    <div class="activity-title">
                        
                        <a href="#projects/1" class="object-link">The status on Sample Project changed to Overdue</a>
                        <span class="object-link deleted-object">The status on Sample Project changed to Overdue</span>
                    </div>
                </div>
            </li>
            
           
            
        </ul>

    
  </div>
        </div>
		
			</div>
			</div>
</div>

<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'index';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';
</script>