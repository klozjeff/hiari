<div id="content" class="content">
    <!-- begin btn-toolbar -->


    <!-- begin btn-group -->
    <div class="btn-group dropdown pull-right">


        <button class="btn btn-primary  p-l-40 p-r-40 btn-sm add_pro_btn pull-right"
                onclick="ajax_set_full('add','<?php echo translate('add_product'); ?>','<?php echo translate('successfully_added!'); ?>','product_add',''); proceed('to_list');">
            <span class="sidebar-icon ti-plus"></span> <?php echo translate('create_product');?>
        </button>
        <button class="btn btn-info  p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
                style="display:none;"  onclick="ajax_set_list();  proceed('to_add');">
            <span class="sidebar-icon ti-arrow-left"></span><?php echo translate('back_to_product_list');?>
        </button>

    </div>


    <!-- end btn-toolbar -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo translate('manage_product_(_physical_)');?></small></h1>
    <!-- end page-header -->

    <!-- begin row -->
    <div class="row">
        <!-- begin col-2 -->
        <div id="list"></div>
    </div>
    <!-- end row -->

</div>
<span id="prod" style="display:none;"></span>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var timer = '<?php $this->benchmark->mark_time(); ?>';
	var user_type = 'vendor';
	var module = 'product';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
	
	function proceed(type){
		if(type == 'to_list'){
			$(".pro_list_btn").show();
			$(".add_pro_btn").hide();
		} else if(type == 'to_add'){
			$(".add_pro_btn").show();
			$(".pro_list_btn").hide();
		}
	}
</script>

