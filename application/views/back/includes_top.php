<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:46 PM
 */
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8" />
    <title><?php echo $system_title;?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />

    <!-- ================== BEGIN BASE CSS STYLE ================== -->

    <link href="<?php echo base_url();?>template/back/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/css/animate.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/css/style.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/css/style-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/css/tsavoerp-custom.css" rel="stylesheet" />
    <!-- ================== END BASE CSS STYLE ================== -->
    <!-- ================== BEGIN PAGE LEVEL STYLE ================== -->
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
   <!-- <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/css/buttons.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/AutoFill/css/autoFill.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/ColReorder/css/colReorder.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/KeyTable/css/keyTable.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/RowReorder/css/rowReorder.bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Select/css/select.bootstrap.min.css" rel="stylesheet" />
-->
    <link href="<?php echo base_url();?>template/back/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url();?>template/back/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <!-- ================== END PAGE LEVEL STYLE ================== -->
    <!--Switchery [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/switchery/switchery.min.css" rel="stylesheet">
    <!--Summernote [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/summernote/summernote.min.css" rel="stylesheet">
    <!--Chosen [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/chosen/chosen.min.css" rel="stylesheet">



    <!--noUiSlider [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/noUiSlider/jquery.nouislider.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>template/back/plugins/noUiSlider/jquery.nouislider.pips.min.css" rel="stylesheet">


    <!--Bootstrap Tags Input [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet">


    <!--Bootstrap Timepicker [ OPTIONAL ]-->
    <link href="<?php echo base_url(); ?>template/back/plugins/Bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet">


    <link href="<?php echo base_url(); ?>template/back/plugins/bootstrap-wizard/css/bwizard.min.css" rel="stylesheet" />

    <!-- ================== BEGIN BASE JS ================== -->
    <!--<script src="<?php echo base_url();?>template/back/plugins/jquery/jquery-1.9.1.min.js"></script>-->
    <script src="<?php echo base_url(); ?>template/back/js/jquery-2.1.1.min.js"></script>
    <script src="<?php echo base_url();?>template/back/plugins/jquery/jquery-migrate-1.1.0.min.js"></script>
    <script src="<?php echo base_url();?>template/back/plugins/jquery-ui/ui/minified/jquery-ui.min.js"></script>
    <script src="<?php echo base_url();?>template/back/plugins/bootstrap/js/bootstrap.min.js"></script>
    <link href="<?php echo base_url(); ?>template/back/plugins/colorpicker/dist/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <!--Page Load Progress Bar [ OPTIONAL ]-->

    <script src="<?php echo base_url(); ?>template/back/plugins/colorpicker/dist/js/bootstrap-colorpicker.js"></script>
    <!-- ================== END BASE JS ================== -->
    <script>
        <?php
        $volume = $this->crud_model->get_type_name_by_id('general_settings','46','value');
        if($this->crud_model->get_type_name_by_id('general_settings','45','value') == 'ok'){
        ?>
        function sound(type){
            document.getElementById(type).volume = <?php if($volume == '10'){ echo 1 ; }else{echo '0.'.round($volume); } ?>;
            document.getElementById(type).play();
        }
        <?php
        } else {
        ?>
        function sound(type){}
        <?php
        }
        ?>
    </script>
</head>
