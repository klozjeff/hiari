<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:47 PM
 */
?>


<!-- ================== BEGIN BASE JS ================== -->


<script src="<?php echo base_url();?>template/back/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/jquery-cookie/jquery.cookie.js"></script>
<!-- ================== END BASE JS ================== -->

<!-- ================== BEGIN PAGE LEVEL JS ================== -->
<script src="<?php echo base_url();?>template/back/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<!--<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/dataTables.buttons.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/buttons.bootstrap.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/buttons.flash.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/jszip.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/pdfmake.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/vfs_fonts.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/buttons.html5.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Buttons/js/buttons.print.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/AutoFill/js/dataTables.autoFill.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/KeyTable/js/dataTables.keyTable.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/RowReorder/js/dataTables.rowReorder.min.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/DataTables/extensions/Select/js/dataTables.select.min.js"></script>

<script src="<?php echo base_url();?>template/back/js/table-manage-combine.demo.min.js"></script>-->
<script src="<?php echo base_url();?>template/back/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="<?php echo base_url();?>template/back/plugins/bootstrap-wizard/js/bwizard.js"></script>
<script src="<?php echo base_url();?>template/back/js/form-wizards.demo.min.js"></script>
<script src="<?php echo base_url();?>template/back/js/apps.min.js"></script>
<!--Bootbox Modals [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/js/activeit.min.js"></script>

<script src="<?php echo base_url();?>template/back/js/ajax_method.js"></script>
<script src="<?php echo base_url(); ?>template/back/plugins/bootbox/bootbox.min.js"></script>
<!--Switchery [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/plugins/switchery/switchery.js"></script>
<!--Summernote [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/plugins/summernote/summernote.js"></script>
<!--Chosen [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/plugins/chosen/chosen.jquery.min.js"></script>



<!--noUiSlider [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/plugins/noUiSlider/jquery.nouislider.all.min.js"></script>


<!--Bootstrap Timepicker [ OPTIONAL ]-->
<script src="<?php echo base_url(); ?>template/back/plugins/Bootstrap-timepicker/bootstrap-timepicker.min.js"></script>

<!-- ================== END PAGE LEVEL JS ================== -->
<script>
    $(document).ready(function() {
        App.init();
        FormWizard.init();
    });
</script>
<?php
$audios = scandir('uploads/audio/admin/');
foreach ($audios as $row) {
    if($row !== '' && $row !== '.' && $row !== '..'){
        $row = str_replace('.mp3', '', $row);
        ?>
        <audio style='display:none;' id='<?php echo $row; ?>' ><source type="audio/mpeg" src="<?php echo base_url(); ?>uploads/audio/admin/<?php echo $row; ?>.mp3"></audio>
        <?php
    }
}
?>
</body>
</html>