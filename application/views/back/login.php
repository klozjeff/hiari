<!DOCTYPE html>
<html style="background:url(<?php echo base_url(); ?>template/back/img/login/login_bg_02.png) 50% 50% / auto repeat scroll;">
<head>
     <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="refresh" content="300">
    <title><?php echo translate('login');?> | <?php echo $this->db->get_where('general_settings',array('type' => 'system_title'))->row()->value;?></title>
    <meta content="" name="Description">
    <meta content="" name="Author">
    <link href="<?php echo base_url();?>template/back/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>template/back/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo  base_url();?>template/back/css/login/login.css" rel="stylesheet">


    <!--jQuery [ REQUIRED ]-->
    <script src="<?php echo base_url(); ?>template/back/js/jquery-2.1.1.min.js"></script>

    <!--BootstrapJS [ RECOMMENDED ]-->
    <script src="<?php echo base_url(); ?>template/back/plugins/bootstrap/js/bootstrap.min.js"></script>


</head>
<body>
    <div id="loginBG01" class="ncsc-login-bg">
        <p class="pngFix"></p>
    </div>
    <div id="loginBG02" class="ncsc-login-bg">
        <p class="pngFix"></p>
    </div>
    <div>
    <div class="ncsc-login-container">
        <div class="ncsc-login-title">
            <h2 style="color: #27A9E3; !important;">Login</h2>
            <span><?php echo translate('sign_in_to_your_account');?><br/>
    登录密码为商城用户通用密码</span></div>
     <div class="panel-body">
         <?php
         echo form_open(base_url() . 'index.php/'.$control.'/login/', array(
             'method' => 'post',
             'id' => 'login',
             'class'=>'form'
         ));
         ?>
         <div class="form-group input">
             <label>Username or Email</label>
             <span class="repuired"></span>
             <input name="email" placeholder="<?php echo translate('email'); ?>" type="text" autocomplete="off" class="form-control text" autofocus>
             <span class="ico"><i class="fa fa-user"></i></span> </div>
         <div class="form-group input">
             <label>Password</label>
             <span class="repuired"></span>
             <input name="password" type="password" placeholder="<?php echo translate('password'); ?>" autocomplete="off" class="form-control text">
             <span class="ico"><i class="fa fa-key"></i></span> </div>
         <div class="input">
             <div class="col-xs-6 text-left">
                 <div class="pad-ver">
                     <a href="#" onclick="ajax_modal('forget_form','<?php echo translate('forget_password'); ?>','<?php echo translate('email_sent_with_new_password!'); ?>','forget','')" class="btn-link mar-rgt" style="color:#27A9E3 !important;"><?php echo translate('forgot_password');?> ?</a>
                 </div>
             </div>
             <div class="col-xs-6">
                 <div class="form-group text-right main_login">
									<span class="snbtn login-submit" onclick="form_submit('login')">
                                    	<?php echo translate('sign_in');?>
                                    </span>
                 </div>
             </div>
  </div>
         </form>

     </div>
    </div>
</div>
    <!--Activeit Admin [ RECOMMENDED ]-->
    <script src="<?php echo base_url(); ?>template/back/js/activeit.min.js"></script>
    <script src="<?php echo base_url(); ?>template/back/plugins/bootbox/bootbox.min.js"></script>
<script type="application/javascript" src="<?php echo base_url();?>template/back/js/ajax_login.js"></script>
    <script>
        var base_url = "<?php echo base_url(); ?>";
        var cancdd = "<?php echo translate('cancelled'); ?>";
        var req = "<?php echo translate('this_field_is_required'); ?>";
        var sing = "<?php echo translate('signing_in...'); ?>";
        var nps = "<?php echo translate('new_password_sent_to_your_email'); ?>";
        var lfil = "<?php echo translate('login_failed!'); ?>";
        var wrem = "<?php echo translate('wrong_e-mail_address!_try_again'); ?>";
        var lss = "<?php echo translate('login_successful!'); ?>";
        var sucss = "<?php echo translate('SUCCESS!'); ?>";
        var rpss = "<?php echo translate('reset_password'); ?>";
        var user_type = "<?php echo $control; ?>";
        var module = "login";
        var unapproved = "<?php echo translate('account_not_approved._wait_for_approval.'); ?>";

        window.addEventListener("keydown", checkKeyPressed, false);
        function checkKeyPressed(e) {
            if (e.keyCode == "13") {
                $('body').find(':focus').closest('form').find('.snbtn').click();
                if($('body').find('.modal-content').find(':focus').closest('form').closest('.modal-content').length > 0){
                    $('body').find('.modal-content').find(':focus').closest('form').closest('.modal-content').find('.snbtn_modal').click();
                }
            }
        }
    </script>

</body>
</html>