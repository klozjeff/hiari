<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:47 PM
 */
?>
<footer class="panel-footer fixed-bottom clearfix">
    <div class="pull-right">
        Developed by <a href="http://www.unikorn.co.ke" target="_blank">Unikorn Technologies Limited</a>
    </div>
    <div class="pull-left">
        <?=date('Y');?>  &copy All rights reserved
    </div>
</footer>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>

