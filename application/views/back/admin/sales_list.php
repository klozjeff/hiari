<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:48 PM
 */
?>
<!-- begin col-10 -->
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Sales List </h4>
        </div>
        <div class="panel-body">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th style="width:4ex"><?php echo translate('ID');?></th>
                    <th><?php echo translate('sale_code');?></th>
                    <th><?php echo translate('buyer');?></th>
                    <th><?php echo translate('date');?></th>
                    <th><?php echo translate('total');?></th>
                    <th><?php echo translate('delivery_status');?></th>
                    <th><?php echo translate('payment_status');?></th>
                    <th class="text-right"><?php echo translate('options');?></th>

                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach($all_sales as $row){
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>#<?php echo $row['sale_code']; ?></td>
                        <td><?php echo $this->crud_model->get_type_name_by_id('user',$row['buyer'],'username'); ?></td>
                        <td><?php echo date('d-m-Y',$row['sale_datetime']); ?></td>
                        <td class="pull-right"><?php echo currency('','def').$this->cart->format_number($row['grand_total']); ?></td>
                        <td>
                            <?php
                            $this->benchmark->mark_time();
                            $delivery_status = json_decode($row['delivery_status'],true);
                            foreach ($delivery_status as $dev) {
                                ?>

                                <div class="label label-<?php if($dev['status'] == 'delivered'){ ?>success<?php } else { ?>danger<?php } ?>">
                                    <?php
                                    if(isset($dev['vendor'])){
                                        echo $this->crud_model->get_type_name_by_id('vendor', $dev['vendor'], 'display_name').' ('.translate('vendor').') : '.$dev['status'];
                                    } else if(isset($dev['admin'])) {
                                        echo translate('admin').' : '.$dev['status'];
                                    }
                                    ?>
                                </div>
                                <br>
                                <?php
                            }
                            ?>
                        </td>
                        <td>

                            <?php
                            $payment_status = json_decode($row['payment_status'],true);
                            foreach ($payment_status as $dev) {
                                ?>

                                <div class="label label-<?php if($dev['status'] == 'paid'){ ?>success<?php } else { ?>danger<?php } ?>">
                                    <?php
                                    if(isset($dev['vendor'])){
                                        echo $this->crud_model->get_type_name_by_id('vendor', $dev['vendor'], 'display_name').' ('.translate('vendor').') : '.$dev['status'];
                                    } else if(isset($dev['admin'])) {
                                        echo translate('admin').' : '.$dev['status'];
                                    }
                                    ?>
                                </div>
                                <br>
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['sale_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                            <span class="sidebar-icon ti-pencil pull-right"  onclick="ajax_modal('delivery_payment','<?php echo translate('delivery_status'); ?>','<?php echo translate('successfully_edited!'); ?>','delivery_payment','<?php echo $row['sale_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00a65a !important"></span>

                            <span class="sidebar-icon ti-files pull-right"  onclick="ajax_set_full('view','<?php echo translate('title'); ?>','<?php echo translate('successfully_edited!'); ?>','sales_view','<?php echo $row['sale_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>



                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<style type="text/css">
    .pending{
        background: #D2F3FF  !important;
    }
    .pending:hover{
        background: #9BD8F7 !important;
    }
</style>
<!-- end col-10 -->
