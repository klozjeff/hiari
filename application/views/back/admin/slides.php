<div id="content" class="content">
    <div class="" style="">

        <div class="" >


            <a style="margin-right:3px"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right"
               onclick="ajax_modal('add','<?php echo translate('add_slides'); ?>','<?php echo translate('successfully_added!');?>','slides_add','')">
                <span class="sidebar-icon ti-plus"></span>  <?php echo translate('create_slides');?>
            </a>

            <div  style="margin-right:50%" class="btn-group dropdown pull-right">

                <button class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Slider Sub Menu <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left text-sm">
                    <li>
                        <a href="<?php echo base_url(); ?>index.php/admin/slider/"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i><?php echo translate('layer_slider');?></a></li>
                    <li class="active"><a href="<?php echo base_url(); ?>index.php/admin/slides/"><i class="fa f-s-10 fa-fw m-r-5"></i><?php echo translate('top_slides');?></a></li>



                </ul>
            </div>
        </div>
        <h1 class="page-header"><?php echo translate('manage_top_carousal_slides');?></h1>
    </div>
    <div class="row" id="list">

    </div>
</div>

<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'slides';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';

    $(document).ready(function(){
        $(".sw").each(function(){
            var h = $(this);
            var id = h.attr('id');
            var set = h.data('set');
            new Switchery(document.getElementById(id), {color:'rgb(100, 189, 99)', secondaryColor: '#cc2424', jackSecondaryColor: '#c8ff77'});
            var changeCheckbox = document.querySelector('#'+id);
            changeCheckbox.onchange = function() {
                //alert($(this).data('id'));
                ajax_load(base_url+'index.php/'+user_type+'/general_settings/'+set+'/'+changeCheckbox.checked,'site','othersd');
                if(changeCheckbox.checked == true){
                    $.activeitNoty({
                        type: 'success',
                        icon : 'fa fa-check',
                        message : s_e,
                        container : 'floating',
                        timer : 3000
                    });
                    sound('published');
                } else {
                    $.activeitNoty({
                        type : 'danger',
                        icon : 'fa fa-check',
                        message : s_d,
                        container : 'floating',
                        timer : 3000
                    });
                    sound('unpublished');
                }
                //alert(changeCheckbox.checked);
            };
        });
    });
</script>


