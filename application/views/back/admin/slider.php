<link rel="stylesheet" href="<?php echo base_url(); ?>template/front/layerslider/css/layerslider.css" type="text/css">
<script src="<?php echo base_url(); ?>template/front/layerslider/js/greensock.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/front/layerslider/js/layerslider.transitions.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>template/front/layerslider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>

<style>
    #layerslider * {
        font-family: 'Roboto', sans-serif;
    }
    body {
        padding: 0 !important;
    }
</style>

<div id="content" class="content">
    <div class="" style="">

        <div class="" >


            <a style="margin-right:3px"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right"
                    onclick="ajax_set_full('add','<?php echo translate('title'); ?>','<?php echo translate('successfully_added!'); ?>','slider_add','')">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_slider');?>
            </a>
            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right"
                    onclick="ajax_set_list()">
                <span class="sidebar-icon ti-view-list-alt"></span> <?php echo translate('slider_list');?>
            </a>
            <a style="margin-right:3px" class="btn btn-success p-l-40 p-r-40 btn-sm pull-right"
                    onclick="ajax_set_full('serial','<?php echo translate('slider_serial'); ?>','<?php echo translate('successfully_serialized!'); ?>','slider_serial',''); ">
                <span class="sidebar-icon ti-view-list"></span> <?php echo translate('slider_serial');?>
            </a>
            <!--<div class="col-sm-6">
                <input id="set_slider" class='sw' data-set='set_slider' type="checkbox" <?php if($this->crud_model->get_type_name_by_id('general_settings','53','value') == 'ok'){ ?>checked<?php } ?> />
            </div>-->
            <div  style="margin-right:25%" class="btn-group dropdown pull-right">

                <button class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    Slider Sub Menu <span class="caret"></span>
                </button>
                <ul class="dropdown-menu text-left text-sm">
                    <li class="active">
                        <a href="<?php echo base_url(); ?>index.php/admin/slider/"><i class="fa fa-circle f-s-10 fa-fw m-r-5"></i><?php echo translate('layer_slider');?></a></li>
                    <li><a href="<?php echo base_url(); ?>index.php/admin/slides/"><i class="fa f-s-10 fa-fw m-r-5"></i><?php echo translate('top_slides');?></a></li>



                </ul>
            </div>
        </div>
        <h1 class="page-header"><?php echo translate('manage_layer_slider');?></h1>
    </div>
    <div class="row" id="list">

    </div>
</div>

<span id="slid"></span>
<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'slider';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';

    $(document).ready(function(){
        $(".sw").each(function(){
            var h = $(this);
            var id = h.attr('id');
            var set = h.data('set');
            new Switchery(document.getElementById(id), {color:'rgb(100, 189, 99)', secondaryColor: '#cc2424', jackSecondaryColor: '#c8ff77'});
            var changeCheckbox = document.querySelector('#'+id);
            changeCheckbox.onchange = function() {
                //alert($(this).data('id'));
                ajax_load(base_url+'index.php/'+user_type+'/general_settings/'+set+'/'+changeCheckbox.checked,'site','othersd');
                if(changeCheckbox.checked == true){
                    $.activeitNoty({
                        type: 'success',
                        icon : 'fa fa-check',
                        message : s_e,
                        container : 'floating',
                        timer : 3000
                    });
                    sound('published');
                } else {
                    $.activeitNoty({
                        type : 'danger',
                        icon : 'fa fa-check',
                        message : s_d,
                        container : 'floating',
                        timer : 3000
                    });
                    sound('unpublished');
                }
                //alert(changeCheckbox.checked);
            };
        });
    });
</script>