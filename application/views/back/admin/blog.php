<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >

            <a style="margin-right:3px" class="btn btn-success p-l-40 p-r-40 btn-sm pull-right"
               href="<?php echo base_url(); ?>index.php/admin/blog_category/">
                <i class="fa fa-list-alt"></i> <?php echo translate('blog_categories');?>
            </a>
            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right add_pro_btn"
               onclick="ajax_set_full('add','<?php echo translate('add_blog'); ?>','<?php echo translate('successfully_added!'); ?>','blog_add',''); proceed('to_list');">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_blog');?>
            </a>
            <a style="margin-right:3px;display: none;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_set_list();  proceed('to_add');">
                <span class="sidebar-icon ti-view-list-alt"></span> <?php echo translate('back_to_blog_list');?>
            </a>

        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_blog');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>

</div>
<span id="prod"></span>
<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'admin';
	var module = 'blog';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
	
	function proceed(type){
		if(type == 'to_list'){
			$(".pro_list_btn").show();
			$(".add_pro_btn").hide();
		} else if(type == 'to_add'){
			$(".add_pro_btn").show();
			$(".pro_list_btn").hide();
		}
	}
</script>

