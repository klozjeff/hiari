<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h1 class="panel-title">Pages List</h1>
        </div>
<div class="panel-body" id="demo_s">
		<table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,6" data-show-toggle="false" data-show-columns="false" data-search="true" >
			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('page_name');?></th>					
					<th><?php echo translate('publish');?></th>		
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
	
			<tbody >
			<?php
				$i=0;
            	foreach($all_page as $row){
            		$i++;
			?>
			<tr>
				<td><?php echo $i; ?></td>
				<td><?php echo $row['page_name']; ?></td>
                <td>
                		<input id="pag_<?php echo $row['page_id']; ?>" class='sw1' type="checkbox" data-id='<?php echo $row['page_id']; ?>' <?php if($row['status'] == 'ok'){ ?>checked<?php } ?> />
                </td>
				<td class="text-right">
                     <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['page_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important">

            </span>

                    <span class="sidebar-icon ti-pencil pull-right"
                          onclick="ajax_set_full('edit','<?php echo translate('edit_page'); ?>','<?php echo translate('successfully_edited!'); ?>','page_edit','<?php echo $row['page_id']; ?>'); proceed('to_list');" data-original-title="Edit" data-container="body"
                          style="font-size:18px;margin-top:10px;color:#00acac !important"></span>

                    <a class="sidebar-icon ti-eye pull-right"
                          href="<?php echo base_url(); ?>index.php/home/page/<?php echo $row['parmalink']; ?>" target="_blank"
                          style="font-size:18px;margin-top:10px;color:#00acac !important"></a>


				</td>
			</tr>
            <?php
            	}
			?>
			</tbody>
		</table>
</div></div>
</div>