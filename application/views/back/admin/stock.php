<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >




            <a style="margin-right:3px;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_modal('destroy','<?php echo translate('destroy_product_entry'); ?>','<?php echo translate('add_stock_entry_taken!'); ?>','stock_destroy','')">
                <span class="sidebar-icon ti-minus"></span> <?php echo translate('reduce_product_quantity');?>
            </a>
            <a style="margin-right:3px;"  class="btn btn-success p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_modal('add','<?php echo translate('add_product_stock'); ?>','<?php echo translate('destroy_entry_taken!'); ?>', 'stock_add', '' )">
                <span class="sidebar-icon ti-plus"></span> <?php echo translate('add_product_quantity');?>
            </a>



        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_product_stock');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>

</div>
<script>
	var base_url 		= '<?php echo base_url(); ?>'
	var user_type 		= 'admin';
	var module 			= 'stock';
	var list_cont_func  = 'list';
	var dlt_cont_func 	= 'delete';
</script>


