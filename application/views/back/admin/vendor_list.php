<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:36 PM
 */
?>
<!-- begin col-10 -->
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Vendor List </h4>
        </div>
        <div class="panel-body">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?php echo translate('logo');?></th>
                    <th><?php echo translate('display_name');?></th>
                    <th><?php echo translate('name');?></th>
                    <th><?php echo translate('status');?></th>

                    <th class="text-right"><?php echo translate('options');?></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $i = 0;
                foreach($all_vendors as $row){
                    $i++;
                    ?>
                    <tr>
                        <td>
                            <?php
                            if(file_exists('uploads/vendor_logo_image/logo_'.$row['vendor_id'].'.png')){
                                ?>
                                <img class="img-sm img-border"
                                     src="<?php echo base_url(); ?>uploads/vendor_logo_image/logo_<?php echo $row['vendor_id']; ?>.png" />
                                <?php
                            } else {
                                ?>
                                <img class="img-sm img-border"
                                     src="<?php echo base_url(); ?>uploads/vendor_logo_image/default.jpg" alt="">
                                <?php
                            }
                            ?>

                        </td>
                        <td><?php echo $row['display_name']; ?></td>
                        <td><?php echo $row['name']; ?></td>
                        <td>
                            <div class="label label-<?php if($row['status'] == 'approved'){ ?>success<?php } else { ?>danger<?php } ?>">
                                <?php echo $row['status']; ?>
                            </div>
                        </td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['vendor_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                            <span class="sidebar-icon ti-wallet pull-right"  onclick="ajax_modal('pay_form','<?php echo translate('pay_vendor'); ?>','<?php echo translate('successfully_viewed!'); ?>','vendor_pay','<?php echo $row['vendor_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#6d7479 !important"></span>

                            <span class="sidebar-icon ti-thumb-up pull-right"  onclick="ajax_modal('approval','<?php echo translate('vendor_approval'); ?>','<?php echo translate('successfully_viewed!'); ?>','vendor_approval','<?php echo $row['vendor_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00a65a !important"></span>

                            <span class="sidebar-icon ti-user pull-right"  onclick="ajax_modal('view','<?php echo translate('view_profile'); ?>','<?php echo translate('successfully_viewed!'); ?>','vendor_view','<?php echo $row['vendor_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>


                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- end col-10 -->
