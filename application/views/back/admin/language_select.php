<div class="col-sm-12 form-horizontal" style="margin-top:9px !important;">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h1 class="panel-title">
                <?php echo $lang.' '.translate('set_translations');?>
            </h1>
        </div>

<div class="panel-body" id="demo_s">

    <table id="data-table" class="table table-bordered table-striped" data-pagination="true" data-show-refresh="true" data-ignorecol="0,2" data-show-toggle="true" data-show-columns="false" data-search="true" >
        <thead>
            <tr>
                <th data-field="no" data-align="left" data-sortable="flase">
                    <?php echo translate('no');?>
                </th>
                <th data-field="word" data-align="center" data-sortable="true">
                    <?php echo translate('word');?>
                </th>
                <th data-field="translation" data-sortable="false">
                    <?php echo translate('translation');?>
                </th>
                <th data-field="options" data-sortable="false" data-align="right">
                    <?php echo translate('options');?>
                </th>
            </tr>
        </thead>
        <tbody>
        <?php
        $i=0;
        foreach ($all_words as $row)
        {
            $i+=1;
            form_open(base_url() . 'index.php/admin/language_settings/upd_trn/'.$row['word_id'], array(
                'class' => 'form-horizontal trs',
                'method' => 'post',
                'id' => $lang.'_'.$row['word_id']
            ));
            ?>
           <tr>
            <td><?php echo $row['word_id']; ?></td>
            <td width="30%"><?php echo str_replace('_', ' ', $row['word']); ?></td>
            <td>

                    <input type="text" name="translation" value="<?php echo $row[$lang];?>" class ="form-control ann"  style="float: left !important;width: 75% !important;"/>
                    <input type="hidden" name="lang" value="<?php echo $lang;?>" />


                <span class="btn btn-info submittera" style="float: right;" data-wid="<?php echo $lang.'_'.$row['word_id'];?>"  data-ing="<?php echo translate('saving');?>" data-msg="<?php echo translate('updated!');?>" ><i class="fa fa-save"></i> <?php echo translate('save');?></span>

            </td>
            <td>   <span style="font-size:18px;margin-top:10px;color:#ff5b57 !important" onclick="delete_lang('<?php echo $row['word_id']; ?>')" class="sidebar-icon ti-trash" data-toggle="tooltip"
                         title=" <?php echo translate('delete');?>" data-container="body">
                            <?php //echo translate('delete_language');?>
                    </span></td>
           </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
    </form>
</div>
    </div>
</div>



<div class="aabn btn"><?php echo translate('translate');?></div>
<div class="ssdd btn"><?php echo translate('save_all');?></div>

<script type="text/javascript">

    $('#list').on('click', '.ssdd', function() {
        $('#data-table').find('form').each(function(){
            var nw = $(this);
            nw.find('.submittera').click();
        });
    });
    
    $('#list').on('click', '.aabn', function() {
        $('#events-table').find('.abv').each(function(index, element) {
            var now = $(this);
            var dtt = now.closest('tr').find('.ann');
            var str = now.html();
            str = str.replace('<font><font>', '');
            str = str.replace('</font></font>', '');
            dtt.val(str);
        });
    });
    var extra = '<?php echo $lang; ?>';

    $(document).ready(function() {
        $('.page-list').find('.dropdown-menu li:last-child').click();
        $('#data-table').DataTable();

    });
</script>
