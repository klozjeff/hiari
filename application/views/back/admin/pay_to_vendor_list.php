<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:44 PM
 */
?>
<!-- begin col-10 -->
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Sales List </h4>
        </div>
        <div class="panel-body">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?php echo translate('logo');?></th>
                    <th><?php echo translate('vendor');?></th>
                    <th><?php echo translate('amount');?></th>
                    <th><?php echo translate('payment_method');?></th>
                    <th><?php echo translate('status');?></th>

                    <th class="text-right"><?php echo translate('options');?></th>
                </tr>
                </thead>
                <tbody >
                <?php
                $i = 0;
                foreach($vendor_payments as $row){
                    $i++;
                    ?>
                    <tr>
                        <td>
                            <?php
                            if(file_exists('uploads/vendor_logo_image/logo_'.$row['vendor_id'].'.png')){
                                ?>
                                <img class="img-sm img-border"
                                     src="<?php echo base_url(); ?>uploads/vendor_logo_image/logo_<?php echo $row['vendor_id']; ?>.png" />
                                <?php
                            } else {
                                ?>
                                <img class="img-sm img-border"
                                     src="<?php echo base_url(); ?>uploads/vendor_logo_image/default.jpg" alt="">
                                <?php
                            }
                            ?>

                        </td>
                        <td><?php echo $this->db->get_where('vendor',array('vendor_id'=>$row['vendor_id']))->row()->display_name; ?></td>
                        <td><?php echo currency('','def').$row['amount']; ?></td>
                        <td>
                            <?php
                            if($row['method'] =='c2'){
                                echo 'Twocheckout';
                            }else{
                                echo $row['method'];
                            }
                            ?>
                        </td>
                        <td>
                            <div class="label label-<?php if($row['status'] == 'paid'){ ?>purple<?php } else { ?>danger<?php } ?>">
                                <?php echo $row['status']; ?>
                            </div>
                            <br>

                        </td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['vendor_invoice_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>


                            <span class="sidebar-icon ti-walllet pull-right"  onclick="ajax_modal('vendor_payment_status','<?php echo translate('vendor_payment_status'); ?>','<?php echo translate('successfully_edited!'); ?>','vendor_payment_status','<?php echo $row['vendor_invoice_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end col-10 -->

