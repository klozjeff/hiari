<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h1 class="panel-title">
                Coupons List
            </h1>
        </div>	<div class="panel-body" id="demo_s">
		<table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" >

			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('title');?></th>
					<th><?php echo translate('code');?></th>
					<th><?php echo translate('added_by');?></th>
					<th><?php echo translate('status');?></th>
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
				
			<tbody >
			<?php
				$i=0;
            	foreach($all_coupons as $row){
            		$i++;
			?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo $row['code']; ?></td>
                    <td>
                    	<?php
                    		$by = json_decode($row['added_by'],true);
                    		$name = $this->crud_model->get_type_name_by_id($by['type'],$by['id'],'name'); 
                    	?>
                    	<?php echo $name; ?> (<?php echo $by['type']; ?>)
                    </td>
		            <td>
		                <input id="pub_<?php echo $row['coupon_id']; ?>" class='sw1' type="checkbox" data-id='<?php echo $row['coupon_id']; ?>' <?php if($row['status'] == 'ok'){ ?>checked<?php } ?> />
		            </td>
                    
                    <td class="text-right">
                        <span style="font-size:18px;margin-top:10px;color:#00acac !important" class="sidebar-icon ti-pencil" data-toggle="tooltip"
                            onclick="ajax_modal('edit','<?php echo translate('edit_coupon'); ?>','<?php echo translate('successfully_edited!'); ?>','coupon_edit','<?php echo $row['coupon_id']; ?>')" 
                                data-original-title="Edit" 
                                    data-container="body"><?php //echo translate('edit');?>
                        </span>
                        
                        <span style="font-size:18px;margin-top:10px;color:#ff5b57 !important" onclick="delete_confirm('<?php echo $row['coupon_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"
                        class="sidebar-icon ti-trash"
                        data-toggle="tooltip" data-original-title="Delete"
                        data-container="body"><?php //echo translate('delete');?>
                        </span>
                        
                    </td>
                </tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>
    <div id="coupn"></div>
	<div id='export-div'>
		<h1 style="display:none;"><?php echo translate('coupon'); ?></h1>
		<table id="export-table" data-name='coupon' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('title');?></th>
						<th><?php echo translate('code');?></th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					$i = 0;
	            	foreach($all_coupons as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
                    <td><?php echo $row['title']; ?></td>
                    <td><?php echo $row['code']; ?></td>
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>
    </div>
</div>
<style>
	.highlight{
		background-color: #E7F4FA;
	}
</style>







           