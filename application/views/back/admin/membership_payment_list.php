<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:07 PM
 */
?>
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">Membership Payments</div>

    <div class="panel-body">
        <table id="data-table" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><?php echo translate('logo');?></th>
                    <th><?php echo translate('vendor');?></th>
                    <th><?php echo translate('amount');?></th>
                    <th><?php echo translate('upgraded_vendor_package');?></th>
                    <th><?php echo translate('status');?></th>

                    <th class="text-right"><?php echo translate('options');?></th>

                </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach($all_membership_payments as $row){
                $i++;
                ?>
                <tr>
                    <td>
                        <?php
                        if(file_exists('uploads/vendor_logo_image/logo_'.$row['vendor'].'.png')){
                            ?>
                            <img class="img-sm img-border"
                                 src="<?php echo base_url(); ?>uploads/vendor_logo_image/logo_<?php echo $row['vendor']; ?>.png" />
                            <?php
                        } else {
                            ?>
                            <img class="img-sm img-border"
                                 src="<?php echo base_url(); ?>uploads/vendor_logo_image/default.jpg" alt="">
                            <?php
                        }
                        ?>

                    </td>
                    <td><?php echo $this->db->get_where('vendor',array('vendor_id'=>$row['vendor']))->row()->display_name; ?></td>
                    <td><?php echo currency('','def').$row['amount']; ?></td>
                    <td><?php echo $this->db->get_where('membership',array('membership_id'=>$row['membership']))->row()->title; ?></td>
                    <td><?php echo $row['status']; ?></td>
                    <td class="text-right">
                        <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['membership_payment_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                        <span class="sidebar-icon ti-eye pull-right"  onclick="ajax_modal('view','<?php echo translate('view_payment_details'); ?>','<?php echo translate('successfully_viewed!'); ?>','membership_payment_view','<?php echo $row['membership_payment_id']; ?>')"
                              style="font-size:18px;margin-top:10px;color:#00acac !important"></span>



                    </td>
                </tr>
                <?php
            }
            ?>

            </tbody>
        </table>
    </div></div>
</div>
