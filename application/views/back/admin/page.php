<div id="content" class="content">
    <div class="" style="" id="page-title">
        <div class="" >



            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right add_pro_btn"
               onclick="ajax_set_full('add','<?php echo translate('add_page'); ?>','<?php echo translate('successfully_added!'); ?>','page_add',''); proceed('to_list');">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_page');?>
            </a>
            <a style="margin-right:3px;display: none;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_set_list();  proceed('to_add');">
                <span class="sidebar-icon ti-view-list-alt"></span> <?php echo translate('back_to_page_list');?>
            </a>

        </div>
        <h1 class="page-header"><?php echo translate('build_responsive_pages');?></h1>


    </div>
    <div class="row" id="list" style="">


</div>
<span id="pag" style="display:none;"></span>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var user_type = 'admin';
	var module = 'page';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
	
	function proceed(type){
		
		if(type == 'to_list'){
			$(".pro_list_btn").show();
			$(".add_pro_btn").hide();
		} else if(type == 'to_add'){
			$(".add_pro_btn").show();
			$(".pro_list_btn").hide();
		} 
	}
</script>

