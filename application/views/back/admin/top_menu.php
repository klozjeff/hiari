
<!-- begin #top-menu -->
<div id="top-menu" class="top-menu">
    <!-- begin top-menu nav -->
    <ul class="nav" style="margin-left:0px !important;">
        <li <?php if($page_name=="dashboard"){?> class="active" <?php } ?>>
            <a href="<?php echo base_url(); ?>index.php/admin/">
                <span class="sidebar-icon ti-home "></span>
                <span><?php echo translate('dashboard');?></span>
            </a>

        </li>
        <?php
        if($physical_check == 'ok' && $digital_check == 'ok') {
            if ($this->crud_model->admin_permission('category') ||
                $this->crud_model->admin_permission('sub_category') ||
                $this->crud_model->admin_permission('brand') ||
                $this->crud_model->admin_permission('product') ||
                $this->crud_model->admin_permission('stock') ||
                $this->crud_model->admin_permission('category_digital') ||
                $this->crud_model->admin_permission('sub_category_digital') ||
                $this->crud_model->admin_permission('digital')
            ) {
                ?>
                <li <?php if($page_name=="category" ||
                    $page_name=="sub_category" ||
                    $page_name=="product" ||
                    $page_name=="stock" ||
                    $page_name=="category_digital" ||
                    $page_name=="sub_category_digital"||
                    $page_name=="digital" ){?>
                    class="has-sub active"
                <?php } else { ?> class="has-sub" <?php } ?>>
                    <a href="javascript:">
                        <b class="caret pull-right"></b>
                        <span class="sidebar-icon ti-briefcase"></span>
                        <span><?php echo translate('products'); ?></span>
                    </a>
                    <ul class="sub-menu">
                        <?php
                        if($this->crud_model->admin_permission('category') ||
                            $this->crud_model->admin_permission('sub_category') ||
                            $this->crud_model->admin_permission('brand') ||
                            $this->crud_model->admin_permission('product') ||
                            $this->crud_model->admin_permission('stock') ) {
                            ?>
                            <li class="has-sub">
                                <a href="javascript:;"><b class="caret pull-right"></b> <?=translate('physical_products');?></a>
                                <ul class="sub-menu">
                                    <?php if($this->crud_model->admin_permission('category')){?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/category"> <?=translate('category');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('brand')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/brand"> <?=translate('brands');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('sub_category')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/sub_category"><?php echo translate('sub-category');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('product')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/product"> <?php echo translate('all_products');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('stock')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/stock"><?php echo translate('product_stock');?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>


                            <?php
                        }

                        if($this->crud_model->admin_permission('category_digital') ||
                            $this->crud_model->admin_permission('sub_category_digital') ||
                            $this->crud_model->admin_permission('digital') ){
                            ?>

                            <li class="has-sub">
                                <a href="javascript:;"><b class="caret pull-right"></b> <?php echo translate('digital_products');?></a>
                                <ul class="sub-menu">
                                    <?php if($this->crud_model->admin_permission('category')){?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/category_digital"> <?=translate('category');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('sub_category')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/sub_category_digital"><?php echo translate('sub-category');?></a></li>
                                    <?php } if($this->crud_model->admin_permission('digital')) { ?>
                                        <li><a href="<?php echo base_url(); ?>index.php/admin/digital"> <?php echo translate('all_digital');?></a></li>
                                    <?php } ?>
                                </ul>
                            </li>


                            <?php
                        }


                        ?>

                    </ul>
                </li>


                <?php
            }
        }
        ?>
        <?php
        if($this->crud_model->admin_permission('sale')){
            ?>
            <li <?php if($page_name=="sales"){?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/admin/sales/">
                    <span class="sidebar-icon ti-wallet "></span>
                    <span><?php echo translate('sale');?></span>
                </a>

            </li>
            <?php
        }

        if($this->crud_model->get_type_name_by_id('general_settings','58','value') == 'ok') {
            if ($this->crud_model->admin_permission('vendor') ||
                $this->crud_model->admin_permission('membership_payment') ||
                $this->crud_model->admin_permission('membership')
            ) {

                ?>

                <li <?php if($page_name=="vendor" ||
                    $page_name=="membership_payment" ||
                    $page_name=="slides_vendor" ||
                    $page_name=="membership" ){ ?> class="has-sub active" <?php } else {?> class="has-sub" <?php } ?>>

                    <a href="javascript:">
                        <b class="caret pull-right"></b>
                        <span class="sidebar-icon ti-user"></span>
                        <span> <?php echo translate('vendors');?></span>
                    </a>
                    <ul class="sub-menu">
                        <li><a href="<?php echo base_url(); ?>index.php/admin/vendor/"> <?php echo translate('vendor_list');?></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/pay_to_vendor/"> <?php echo translate('pay_to_vendor');?></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/membership_payment/"> <?php echo translate('package_payments');?></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/membership/"> <?php echo translate('vendor_packages');?></a></li>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/slides/vendor"><?php echo translate('vendor\'s_slides');?></a></li>


                    </ul>
                </li>

                <?php
            }
        }

        if($this->crud_model->admin_permission('user')) {
            ?>
            <li <?php if ($page_name == "user") { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>index.php/admin/user/">
                    <i class="fa fa-users"></i>
                    <span><?php echo translate('customers'); ?></span>
                </a>

            </li>

            <?php
        }
        if($this->crud_model->admin_permission('report')){
            ?>
            <li <?php if($page_name=="report" ||
                $page_name=="report_stock" ||
                $page_name=="report_wish" ){?> class="has-sub active" <?php } else {?> class="has-sub" <?php }?>>
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <span class="sidebar-icon ti-stats-up"></span>
                    <span><?php echo translate('reports');?></span>
                </a>
                <ul class="sub-menu">
                    <li><a href="<?php echo base_url(); ?>index.php/admin/report/">  <?php echo translate('product_compare');?></a></li>
                    <?php if($physical_check=='ok') {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/report_stock/"> <?php echo translate('product_stock');?></a></li>
                    <?php }?>
                    <li><a href="<?php echo base_url(); ?>index.php/admin/report_wish/"> <?php echo translate('product_wishes');?></a></li>

                </ul>

            </li>

            <?php
        }
        if($this->crud_model->admin_permission('slider') ||
            $this->crud_model->admin_permission('slides') ||
            $this->crud_model->admin_permission('display_settings') ||
            $this->crud_model->admin_permission('site_settings') ||
            $this->crud_model->admin_permission('email_template') ||
            $this->crud_model->admin_permission('captha_n_social_settings') ||
            $this->crud_model->admin_permission('page') ||
            $this->crud_model->admin_permission('business_settings') ||
            $this->crud_model->admin_permission('role') ||
            $this->crud_model->admin_permission('admin')){
            ?>
            <li <?php  if($page_name=="slider" ||
                $page_name=="slides" ||
                $page_name=="display_settings"||
                $page_name=="site_settings" ||
                $page_name=="email_template" ||
                $page_name=="captha_n_social_settings" ||
                $page_name=="default_images" ||
                $page_name=="page"||
                $page_name=="role" ||
                $page_name=="admin" ||
                $page_name=="activation" ||
                $page_name=="payment_method" ||
                $page_name=="curency_method" ||
                $page_name=="faq_settings"){?> class="has-sub active" <?php } else {?> class="has-sub" <?php }?>>
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <span class="sidebar-icon ti-settings"></span>
                    <span>Admin & Settings</span>
                </a>
                <ul class="sub-menu">

                    <li class="has-sub">
                        <a href="javascript:;"><b class="caret pull-right"></b><?php echo translate('frontend_settings');?></a>
                        <ul class="sub-menu">
                            <?php if($this->crud_model->admin_permission('slider') || $this->crud_model->admin_permission('slides')){?>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/slider"> <?=translate('slider_settings');?></a></li>
                            <?php } if($this->crud_model->admin_permission('display_settings')) { ?>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/display_settings/home"> <?=translate('display_settings');?></a></li>
                            <?php } if($this->crud_model->admin_permission('site_settings') || $this->crud_model->admin_permission('email_template') || $this->crud_model->admin_permission('captha_n_social_settings')) { ?>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/site_settings/general_settings/"><?php echo translate('site_settings');?></a></li>
                            <?php } if($this->crud_model->admin_permission('page')) { ?>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/page"> <?php echo translate('build_responsive_pages');?></a></li>
                            <?php } if($this->crud_model->admin_permission('default_images')) { ?>
                                <li><a href="<?php echo base_url(); ?>index.php/admin/default_images"><?php echo translate('set_default_images');?></a></li>
                            <?php } ?>
                        </ul>
                    </li>
                    <?php
                    if($this->crud_model->admin_permission('business_settings'))
                    {
                        ?>

                        <li class="has-sub"><a href="javascript:;"><b
                                    class="caret pull-right"></b><?php echo translate('business_settings'); ?></a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/admin/activation/"> <?= translate('activation'); ?></a>
                                </li>

                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/admin/payment_method/"> <?= translate('payment_method'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/admin/curency_method/"><?php echo translate('currency_'); ?></a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url(); ?>index.php/admin/faqs/"> <?php echo translate('faqs'); ?></a>
                                </li>


                            </ul>

                        </li>
                        <?php
                    }

                    if($this->crud_model->admin_permission('role') ||
                        $this->crud_model->admin_permission('admin') ) {
                        ?>

                        <li class="has-sub">
                            <a href="javascript:;"> <b
                                    class="caret pull-right"></b><?php echo translate('staffs'); ?></a>
                            <ul class="sub-menu">
                                <?php if($this->crud_model->admin_permission('admin')){?>
                                    <li><a href="<?php echo base_url(); ?>index.php/admin/admins/"> <?=translate('all_staffs');?></a></li>
                                <?php } if($this->crud_model->admin_permission('role')) { ?>
                                    <li><a href="<?php echo base_url(); ?>index.php/admin/role/"> <?=translate('staff_permissions');?></a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php
                    }
                    ?>

                </ul>
            </li>
            <?php
        }
        if($this->crud_model->admin_permission('newsletter') ||
            $this->crud_model->admin_permission('contact_message')||
            $this->crud_model->admin_permission('ticket')){
            ?>
            <li <?php if($page_name=="ticket" ||
                $page_name=="newsletter" ||
                $page_name=="contact_message" ){?> class="has-sub active" <?php } else {?> class="has-sub" <?php }?>>
                <a href="javascript:">
                    <b class="caret pull-right"></b>
                    <span class="sidebar-icon ti-email"></span>
                    <span><?php echo translate('messaging');?></span>
                </a>
                <ul class="sub-menu">
                    <?php  if($this->crud_model->admin_permission('newsletter')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/newsletter"> <?php echo translate('newsletters');?></a></li>
                    <?php }  if($this->crud_model->admin_permission('contact_message')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/contact_message"><?php echo translate('contact_messages');?></a></li>
                    <?php }  if($this->crud_model->admin_permission('ticket')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/ticket"> <?php echo translate('ticket');?></a></li>
                    <?php } ?>
                </ul>

            </li>

            <?php
        }
        if($this->crud_model->admin_permission('seo')||
            $this->crud_model->admin_permission('language') ||
            $this->crud_model->admin_permission('blog') ||
            $this->crud_model->admin_permission('coupon')){
            ?>

            <li  <?php if($page_name=="coupon" ||
                $page_name=="blog" ||
                $page_name=="blog_category" ||
                $page_name=="seo_settings" ||
                $page_name=="language"){?> class="has-sub active" <?php } else {?> class="has-sub" <?php }?>>
                <a href="javascript:">
                    <span class="sidebar-icon ti-more-alt"></span>
                    <span>More</span>
                </a>
                <ul class="sub-menu">
                    <?php if ($this->crud_model->admin_permission('coupon')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/coupon/">  <?php echo translate('discount_coupon');?></a></li>
                    <?php } if ($this->crud_model->admin_permission('language')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/language_settings">  <?php echo translate('language');?></a></li>
                    <?php } if ($this->crud_model->admin_permission('seo')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/seo_settings"> SEO </a></li>
                    <?php } if ($this->crud_model->admin_permission('blog')) {?>
                        <li><a href="<?php echo base_url(); ?>index.php/admin/blog">  <?php echo translate('blog');?></a></li>
                    <?php } ?>

                </ul>
            </li>

        <?php }
        ?>



        <!-- <li class="menu-control menu-control-left">
             <a href="#" data-click="prev-menu"><i class="fa fa-angle-left"></i></a>
         </li>
         <li class="menu-control menu-control-right">
             <a href="#" data-click="next-menu"><i class="fa fa-angle-right"></i></a>
         </li>-->
    </ul>
    <!-- end top-menu nav -->
</div>
