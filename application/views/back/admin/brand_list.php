<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Category List </h4>
        </div>	<div class="panel-body" id="demo_s">
		<table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,4" data-show-toggle="true" data-show-columns="false" data-search="true" >

			<thead>
				<tr>
					<th><?php echo translate('no');?></th>
					<th><?php echo translate('logo');?></th>
					<th><?php echo translate('name');?></th>
					<th class="text-right"><?php echo translate('options');?></th>
				</tr>
			</thead>
				
			<tbody >
			<?php
				$i=0;
            	foreach($all_brands as $row){
            		$i++;
			?>
                <tr>
                    <td><?php echo $i; ?></td>
                    <td >
                        <?php
							if(file_exists('uploads/brand_image/'.$row['logo'])){
						?>
						<img class="img-md" src="<?php echo base_url(); ?>uploads/brand_image/<?php echo $row['logo']; ?>" />  
						<?php
							} else {
						?>
						<img class="img-md" src="<?php echo base_url(); ?>uploads/brand_image/default.jpg" />
						<?php
							}
						?> 
                    </td>
                    <td><?php echo $row['name']; ?></td>
                    <td class="text-right">
                        <span style="font-size:18px;margin-top:10px;color:#00acac !important" class="sidebar-icon ti-pencil" data-toggle="tooltip"
                            onclick="ajax_modal('edit','<?php echo translate('edit_brand_(_physical_product_)'); ?>','<?php echo translate('successfully_edited!'); ?>','brand_edit','<?php echo $row['brand_id']; ?>')" 
                                title="<?php echo translate('edit');?>"
                                    data-container="body">
                        </span>
                        
                        <span style="font-size:18px;margin-top:10px;color:#ff5b57 !important" onclick="delete_confirm('<?php echo $row['brand_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"
                            class="sidebar-icon ti-trash"
                                data-toggle="tooltip" title="<?php echo translate('delete');?>"
                                    data-container="body">
                        </span>
                        
                    </td>
                </tr>
            <?php
            	}
			?>
			</tbody>
		</table>
	</div>
           
	<div id='export-div'>
		<h1 style="display:none;"><?php echo translate('brand'); ?></h1>
		<table id="export-table" data-name='brand' data-orientation='p' style="display:none;">
				<thead>
					<tr>
						<th><?php echo translate('no');?></th>
						<th><?php echo translate('name');?></th>
						<th><?php echo translate('category');?></th>
					</tr>
				</thead>
					
				<tbody >
				<?php
					$i = 0;
	            	foreach($all_brands as $row){
	            		$i++;
				?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $row['name']; ?></td>
					<td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></td>
				</tr>
	            <?php
	            	}
				?>
				</tbody>
		</table>
	</div>
    </div>
<style>
	.highlight{
		background-color: #E7F4FA;
	}
</style>







           