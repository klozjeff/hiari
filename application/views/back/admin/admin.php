<div id="content" class="content">

    <div class="" style="">
        <!-- begin btn-group -->
        <div class="btn-group dropdown pull-right">

            <button class="btn btn-success p-l-40 p-r-40 btn-sm pull-right" onclick="ajax_modal('add','<?php echo translate('add_staff)'); ?>','<?php echo translate('successfully_added!'); ?>','admin_add','')">
                <?php echo translate('create_admin');?>
            </button>


        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_staffs');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>

</div>
<script>
	var base_url = '<?php echo base_url(); ?>';
	var user_type = 'admin';
	var module = 'admins';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>
