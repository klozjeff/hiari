<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:39 PM
 */
?>
<!-- begin #content -->
<div id="content" class="content">
    <!-- begin btn-toolbar -->

<div class="" style="">
        <!-- begin btn-group -->
        <div class="btn-group dropdown pull-right">

            <button class="btn btn-success p-l-40 p-r-40 btn-sm pull-right" onclick="ajax_modal('add','<?php echo translate('add_category_(_physical_product_)'); ?>','<?php echo translate('successfully_added!'); ?>','category_add','')">
                <?php echo translate('create_category');?>
            </button>


        </div>


    <!-- end btn-toolbar -->
    <!-- begin page-header -->
    <h1 class="page-header"><?php echo translate('manage_categories_(_physical_product_)');?></small></h1>
    <!-- end page-header -->
</div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>
    <!-- end row -->
</div>
<!-- end #content -->


<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'category';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';
</script>
