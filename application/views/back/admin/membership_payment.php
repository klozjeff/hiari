<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:06 PM
 */
?>
<div id="content" class="content">
    <div class="" style="">
        <h1 class="page-header"><?php echo translate('manage_vendor_payments')?></h1>
    </div>
    <div class="row" id="list">

    </div>
</div>

<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'membership_payment';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';
</script>
<script src="https://checkout.stripe.com/checkout.js"></script>

