<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >

            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right"
               onclick="ajax_modal('add','<?php echo translate('add_sub-category_(_digital_product_)'); ?>','<?php echo translate('successfully_added!'); ?>','sub_category_add_digital','')">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_sub_category');?>
            </a>



        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_sub_categories_(_digital_product_)');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>

</div>

<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'admin';
	var module = 'sub_category_digital';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>
