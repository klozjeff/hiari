<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">Layer Slide Setting</div>
        <div class="panel-body">
            <table id="data-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th style="width:4ex"><?php echo translate('ID');?></th>
                    <th><?php echo translate('title');?></th>
                    <th><?php echo translate('image');?></th>
                    <th><?php echo translate('publish');?></th>
                    <th class="text-right"><?php echo translate('options');?></th>
                </tr>
                </thead>
                <tbody >
                <?php
                $i = 0;
                foreach($all_slider as $row){
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['title']; ?></td>
                        <td>
                            <img  class="img-responsive thumbnail img-slider mr-none" src="<?php echo base_url(); ?>uploads/slider_image/background_<?php echo $row['slider_id']; ?>.jpg" />
                        </td>
                        <td>
                            <input id="sli_<?php echo $row['slider_id']; ?>" class='sw1' type="checkbox" data-id='<?php echo $row['slider_id']; ?>' <?php if($row['status'] == 'ok'){ ?>checked<?php } ?> />
                        </td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['slider_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                            <span class="sidebar-icon ti-pencil pull-right"    onclick="ajax_set_full('edit','<?php echo translate('title'); ?>','<?php echo translate('successfully_edited!'); ?>','slider_edit','<?php echo $row['slider_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>


                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>

            </table>
        </div>
    </div>
</div>