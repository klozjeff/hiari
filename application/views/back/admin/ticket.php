<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >




            <a style="margin-right:3px;display: none;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               onclick="ajax_set_list();  proceed('to_add');">
                <span class="sidebar-icon ti-view-list-alt"></span> <?php echo translate('back_to_ticket_list');?>
            </a>

        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_ticket');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>

</div>
<span id="prod" style="display:none;"></span>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var user_type = 'admin';
    var module = 'ticket';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';

    function proceed(type) {
        if (type == 'to_list') {
            $(".pro_list_btn").show();
        } else if (type == 'to_add') {
            $(".pro_list_btn").hide();
        }
    }
	function set_summer(){
        $('.summernotes').each(function() {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
            now.closest('div').append('<input type="hidden" class="val" name="'+n+'">');
            now.summernote({
                height: h,
                onChange: function() {
                    now.closest('div').find('.val').val(now.code());
                }
            });
            now.closest('div').find('.val').val(now.code());
        });
	}
</script>

