<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:05 PM
 */
?>
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <div class="panel-title">Packages List</div>
        </div>

        <div class="panel-body">
            <table id="data-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><?php echo translate('no');?></th>
                        <th><?php echo translate('seal');?></th>
                        <th><?php echo translate('title');?></th>
                        <th><?php echo translate('price');?></th>
                        <th><?php echo translate('for');?></th>
                        <th class="text-right"><?php echo translate('options');?></th>

                    </tr>
                </thead>
                <tbody>
                <tr>
                    <td><?php echo 1; ?></td>
                    <td>
                        <img class="img-md img-circle"
                             src="<?php echo $this->crud_model->file_view('membership',0,'100','','thumb','src','','','.png') ?>"  />
                    </td>
                    <td>Free (Default)</td>
                    <td><?php echo currency('','def').'0'; ?></td>
                    <td><?php echo translate('lifetime');?></td>
                    <td class="text-right">
                         <span class="sidebar-icon ti-pencil pull-right"  onclick="ajax_modal('default','<?php echo translate('edit_vendor_package'); ?>','<?php echo translate('successfully_edited!'); ?>','membership_edit',0)"
                               style="font-size:18px;margin-top:10px;color:#00acac !important"></span>

                    </td>
                </tr>
                    <?php
                    $i=1;
                    foreach($all_memberships as $row) {
                        $i++;

                        ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td >
                                <img class="img-md img-circle"
                                     src="<?php echo $this->crud_model->file_view('membership',$row['membership_id'],'100','','thumb','src','','','.png') ?>"  />
                            </td>
                            <td><?php echo $row['title']; ?></td>
                            <td><?php echo currency('','def').$row['price']; ?></td>
                            <td><?php echo $row['timespan']; ?> <?php echo translate('days');?></td>
                            <td class="text-right">
                                <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['membership_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                                <span class="sidebar-icon ti-pencil pull-right"  onclick="ajax_modal('edit','<?php echo translate('edit_vendor_package'); ?>','<?php echo translate('successfully_edited!'); ?>','membership_edit','<?php echo $row['membership_id']; ?>')"
                                      style="font-size:18px;margin-top:10px;color:#00acac !important"></span>


                            </td>
                        </tr>


                        <?php
                    }

                ?>
                </tbody>

            </table>

        </div>
    </div>
</div>