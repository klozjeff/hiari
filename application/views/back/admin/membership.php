<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:04 PM
 */
?>
<div id="content" class="content">
    <div class="" style="">
        <div class="btn-group  pull-right">

            <button class="btn btn-success p-l-40 p-r-40 btn-sm pull-right" onclick="ajax_modal('add','<?php echo translate('add_vendor_package)'); ?>','<?php echo translate('successfully_added!'); ?>','membership_add','')">
                <?php echo translate('create_vendor_package');?>
            </button>


        </div>
        <h1 class="page-header"><?php echo translate('manage_vendor_package');?></h1>
    </div>
    <div class="row" id="list"></div>
</div>
<script>
    var base_url='<?php echo base_url();?>';
    var user_type='admin';
    var module='membership';
    var list_cont_func='list';
    var dlt_cont_func='delete';
</script>

