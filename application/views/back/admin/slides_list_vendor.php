<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">Vendor Slides</div>
        <div class="panel-body">
            <table id="data-table" class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th><?php echo translate('no');?></th>
                    <th><?php echo translate('image');?></th>
                    <th><?php echo translate('button');?></th>
                    <th><?php echo translate('added_by');?></th>
                    <th class="text-right"><?php echo translate('options');?></th>

                </tr>
                </thead>
                <tbody>
                <?php
                $i=0;
                foreach($all_slidess as $row){
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td >
                            <img class="img-md"
                                 src="<?php echo $this->crud_model->file_view('slides',$row['slides_id'],'100','','thumb','src','','','.jpg') ?>"  style="width:120px;"/>
                        </td>
                        <td>
                            <?php if($row['button_text']!=NULL){ ?>
                                <a class="btn btn-xs" style="background:<?php echo $row['button_color']; ?>; color:<?php echo $row['text_color']; ?>" href="<?php echo $row['button_link']; ?>"
                                   data-toggle="tooltip" title="<?php echo translate('click_to_check_link');?>">
                                    <?php echo $row['button_text']; ?>
                                </a>
                            <?php } ?>
                        </td>
                        <td >
                            <?php
                            $added_by = json_decode($row['added_by'],true);
                            echo $this->crud_model->get_type_name_by_id('vendor',$added_by['id'],'display_name');
                            ?>
                        </td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['slides_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>


                        </td>
                    </tr>
                    <?php
                }
                ?>

                </tbody>
            </table>
        </div>
    </div>
</div>