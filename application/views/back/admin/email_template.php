<div id="content" class="content">
    <div class="" style="" id="page-title">
    <div class="" >

        <div  style="margin-right:70%" class="btn-group dropdown pull-right">

            <button class="btn btn-white btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Sub Menu <span class="caret"></span>
            </button>
            <ul class="dropdown-menu text-left text-sm">
                <?php
                if($this->crud_model->admin_permission('site_settings')){
                    ?>
                    <li <?php if ($page_name=='site_settings') {?> class="active" <?php } ?>>
                        <a href="<?php echo base_url(); ?>index.php/admin/site_settings/general_settings"><i class="fa f-s-10 fa-fw m-r-5"></i><?php echo translate('general_settings');?></a></li>
                <?php }
                if($this->crud_model->admin_permission('email_template')) {
                    ?>
                    <li <?php if ($page_name == 'email_template') { ?> class="active" <?php } ?>>
                        <a href="<?php echo base_url(); ?>index.php/admin/email_template/"><i
                                    class="fa f-s-10 fa-fw m-r-5"></i><?php echo translate('email_templates'); ?>
                        </a></li>

                    <?php
                }
                if($this->crud_model->admin_permission('captha_n_social_settings')){
                    ?>
                    <li <?php if ($page_name=='captha_n_social_settings') {?> class="active" <?php } ?>>
                        <a href="<?php echo base_url(); ?>index.php/admin/captha_n_social_settings"><i class="fa f-s-10 fa-fw m-r-5"></i><?php echo translate('third_party_settings');?></a></li>
                <?php } ?>


            </ul>
        </div>
    </div>
    <h1 class="page-header text-overflow"><?php if($page_name=='site_settings'){ echo translate('site_settings'); }
        elseif($page_name=='email_template'){ echo translate('email_templates'); } elseif($page_name=='captha_n_social_settings'){echo translate('third_party_settings');} ?></h1>
</div>

    <div class="tab-base">
        <div class="panel panel-inverse">
            <div class="tab-base tab-stacked-left horizontal-tab">
                <ul class="nav nav-tabs">
                <?php
					$i=0;
                	foreach($table_info as $row1){
						$i++;
				?>
                    <li class="template_tab <?php if($i==1){ ?>active<?php } ?>">
                        <a data-toggle="tab" href="#demo-stk-lft-tab-<?php echo $row1['email_template_id']; ?>"><?php echo translate($row1['title']);?></a>
                    </li>
                <?php
					}
				?>
                </ul>

                <div class="tab-content bg_grey">
                <?php
					$j=0;
                	foreach($table_info as $row2){
						$j++;
				?>	
                    <div id="demo-stk-lft-tab-<?php echo $row2['email_template_id']; ?>" class="tab-pane fade <?php if($j==1){ ?>active in<?php } ?>">
                        <div class="">
                            <div class="panel-heading">
                                <h3 class="panel-title"><?php //echo translate($row2['title']);?></h3>
                            </div>
							<?php
                                echo form_open(base_url() . 'index.php/admin/email_template/update/'.$row2['email_template_id'], array(
                                    'class' => 'form-horizontal',
                                    'method' => 'post',
                                    'id' => '',
                                    'enctype' => 'multipart/form-data'
                                ));
                            ?>
                                <div class="panel-body">
                                	<div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('subject');?></label>
                                        <div class="col-sm-6">
                                            <input type="text" name="subject" value="<?php echo $row2['subject']; ?>"  class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"><?php echo translate('email_body');?></label>
                                        <div class="col-sm-6">
                                            <textarea class="summernotes" data-height='500' data-name='body' ><?php echo $row2['body']; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label"></label>
                                        <div class="col-sm-6" style="color:#C00;">
                                        	**<?php echo translate('N.B');?> : <?php echo translate('do_not_change_the_variables_like');?> [[ ____ ]].
                                        </div>
                                    </div>
                                </div>
                                <div class="text-right">
                                    <span class="btn btn-info submitter"  data-ing='<?php echo translate('saving'); ?>' data-msg='<?php echo translate('settings_updated!'); ?>'>
                                     <span class="sidebar-icon ti-save"></span> <?php echo translate('update');?>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php
					}
				?>    
                </div>
            </div>
        </div>
    </div>
</div>
<div style="display:none;" id="site"></div>
<!-- for logo settings -->
<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'logo_settings';
    var list_cont_func = 'show_all';
    var dlt_cont_func = 'delete_logo';
	
	$(document).ready(function() {
        $('.summernotes').each(function() {
            var now = $(this);
            var h = now.data('height');
            var n = now.data('name');
            now.closest('div').append('<input type="hidden" class="val" name="'+n+'">');
            now.summernote({
                height: h,
                onChange: function() {
                    now.closest('div').find('.val').val(now.code());
                }
            });
			now.closest('div').find('.val').val(now.code());
			now.focus();
        });
	});

	$(document).ready(function() {
		$("form").submit(function(e){
			return false;
		});
	
	});
</script>

<style>

    .nav-tabs {
        background: #fff !important;
        -webkit-border-radius: 5px 5px 0 0;
        -moz-border-radius: 5px 5px 0 0;
        border-radius: 5px 5px 0 0;
    }
    .horizontal-tab{
        margin:15px !important;
        background:#fff !important;
    }
    .horizontal-tab .nav-tabs{
        border: 0 !important;
        display:block !important;
        width:100% !important;
    }
    .horizontal-tab .nav-tabs li{
        float:left !important;
        background:#fff !important;
    }
    .horizontal-tab .nav-tabs li:hover{
        border-bottom:2px solid #dadada !important;
    }
    .horizontal-tab .nav-tabs li.active{
        border-bottom:2px solid #489eed !important;
    }
    .horizontal-tab .nav-tabs li.active a{
        background:#FFF !important;
    }
    .horizontal-tab .nav-tabs>li:not(.active) a:hover {
        border-color:#fff !important;
    }
    .horizontal-tab .tab-content{
        display:block !important;
    }
    </style>
