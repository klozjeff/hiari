<div class="col-md-12">
    <div class="panel panel-heading">
        <div class="panel-heading">
            <h1 class="panel-title">Digital Products</h1>
        </div>
<div class="panel-body" id="demo_s">
    <table id="data-table" class="table table-bordered table-striped" data-pagination="true" data-page-list="[5, 10, 20, 50, 100, 200]"  data-show-refresh="true" data-search="true"  data-show-export="true" >
        <thead>
            <tr>
                <th>SI.No</th>
                <th data-field="image" data-align="right" data-sortable="true">
                    <?php echo translate('image');?>
                </th>
                <th data-field="title" data-align="center" data-sortable="true">
                    <?php echo translate('title');?>
                </th>
                <th data-field="deal" data-sortable="false">
                    <?php echo translate("today's_deal");?>
                </th>
                <th data-field="publish" data-sortable="false">
                    <?php echo translate('publish');?>
                </th>
                <th data-field="featured" data-sortable="false">
                    <?php echo translate('featured');?>
                </th>
                <th data-field="options" data-sortable="false" data-align="right">
                    <?php echo translate('options');?>
                </th>
            </tr>
        </thead>
        <tbody>
        <?php
        $i=0;
        foreach ($all_products as $row) {
            $i++;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><img class="img-sm" style="height:auto !important; border:1px solid #ddd;padding:2px; border-radius:2px !important;" src="<?php echo $this->crud_model->file_view('product',$row['product_id'],'','','thumb','src','multi','one');?>"> </td>
                <td><?php echo $row['title']; ?></td>

                <?php
                if($row['deal'] == 'ok'){ ?>
                    <td><input id="deal_<?php echo $row['product_id'];?>" class="sw2" type="checkbox" data-id="<?php echo $row['product_id'];?>" checked ></td>
                <?php } else { ?>
                    <td><input id="deal_<?php echo $row['product_id'];?>" class="sw2" type="checkbox" data-id="<?php echo $row['product_id'];?>" ></td>
                <?php }
                ?>
                <?php
                if($row['status'] == 'ok'){ ?>
                    <td><input id="pub_<?php echo $row['product_id'];?>" class="sw1" type="checkbox" data-id="<?php echo $row['product_id'];?>" checked /></td>
                <?php } else { ?>
                    <td><input id="pub_<?php echo $row['product_id'];?>" class="sw1" type="checkbox" data-id="<?php echo $row['product_id'];?>" /></td>
                <?php }
                ?>
                <?php
                if($row['featured'] == 'ok'){ ?>
                    <td><input id="fet_<?php echo $row['product_id'];?>" class="sw3" type="checkbox" data-id="<?php echo $row['product_id'];?>" checked /></td>
                <?php } else { ?>
                    <td><input id="fet_<?php echo $row['product_id'];?>" class="sw3" type="checkbox" data-id="<?php echo $row['product_id'];?>" /></td>
                <?php }
                ?>
                <td class="text-right">
 <span class="sidebar-icon ti-eye"
       onclick="ajax_set_full('view','<?php echo translate('view_product');?>','<?php echo translate('successfully_viewed!');?>','digital_view','<?php echo $row['product_id'];?>');proceed('to_list');"
       style="font-size:18px;margin-top:10px;color:#00acac !important" title="<?php echo translate('view');?>"></span>
                    <span class="sidebar-icon ti-gift"
                          onclick="ajax_modal('add_discount','<?php echo translate('view_discount');?>','<?php echo translate('viewing_discount!');?>','add_discount','<?php echo $row['product_id'];?>');"
                          style="font-size:18px;margin-top:10px;color:#8f60b6 !important" title="<?php echo translate('discount');?>"></span>
                    <span class="sidebar-icon ti-download"
                          onclick="digital_download('<?php echo $row['product_id'];?>');"
                          style="font-size:18px;margin-top:10px;color:#18af92 !important" title="<?php echo translate('download');?>"></span>

                    <span class="sidebar-icon ti-pencil"
                          onclick="ajax_set_full('edit','<?php echo translate('edit_product_(_digital_product_)');?>','<?php echo translate('successfully_edited!');?>','digital_edit','<?php echo $row['product_id'];?>');proceed('to_list');"
                          style="font-size:18px;margin-top:10px;color:#00a65a !important" title="<?php echo translate('edit');?>"></span>
                    <span class="sidebar-icon ti-trash" onclick="delete_confirm('<?php echo $row['product_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important" title="<?php echo translate('delete');?>">

            </span>


                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
</div>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#data-table').DataTable();

    });


</script>

