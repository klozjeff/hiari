<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 1:23 PM
 */
?>

<!-- begin col-10 -->
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Sales List </h4>
        </div>
        <div class="panel-body">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?php echo translate('no');?></th>
                    <th><?php echo translate('image');?></th>
                    <th><?php echo translate('name');?></th>
                    <th><?php echo translate('total_purchase');?></th>
                    <th class="text-right"><?php echo translate('options');?></th>
                </tr>
                </thead>
                <tbody >
                <?php
                $i = 0;
                foreach($all_users as $row){
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td>
                            <img class="img-sm img-circle img-border"
                                <?php if(file_exists('uploads/user_image/user_'.$row['user_id'].'.jpg')){ ?>
                                    src="<?php echo base_url(); ?>uploads/user_image/user_<?php echo $row['user_id']; ?>.jpg"
                                <?php } else if($row['fb_id'] !== ''){ ?>
                                    src="https://graph.facebook.com/<?php echo $row['fb_id']; ?>/picture?type=large" data-im='fb'
                                <?php } else if($row['g_id'] !== ''){ ?>
                                    src="<?php echo $row['g_photo']; ?>"
                                <?php } else { ?>
                                    src="<?php echo base_url(); ?>uploads/user_image/default.jpg"
                                <?php } ?>  />
                        </td>
                        <td><?php echo $row['username']; ?></td>
                        <td class="text-right"><?php echo currency('','def').$this->crud_model->total_purchase($row['user_id']); ?></td>
                        <td class="text-right">
                            <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['user_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>


                            <span class="sidebar-icon ti-user pull-right"  onclick="ajax_modal('view','<?php echo translate('view_profile'); ?>','<?php echo translate('successfully_viewed!'); ?>','user_view','<?php echo $row['user_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>


                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end col-10 -->


