<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:43 PM
 */

?>
<!-- begin #content -->
<div id="content" class="content">
    <!-- begin btn-toolbar -->


        <!-- begin btn-group -->
        <div class="btn-group dropdown pull-right">

            <button class="btn btn-success p-l-40 p-r-40 btn-sm pull-right" onclick="ajax_modal('add','<?php echo translate('add_category_(_digital_product_)'); ?>','<?php echo translate('successfully_added!'); ?>','category_add_digital','')">
                <?php echo translate('create_category');?>
</button>


</div>


<!-- end btn-toolbar -->
<!-- begin page-header -->
<h1 class="page-header"><?php echo translate('manage_categories_(_digital_product_)');?></small></h1>
<!-- end page-header -->

<!-- begin row -->
<div class="row">
    <!-- begin col-2 -->
    <div id="list">

    </div>
</div>
<!-- end row -->
</div>
<!-- end #content -->


<script>
    var base_url = '<?php echo base_url(); ?>'
    var user_type = 'admin';
    var module = 'category_digital';
    var list_cont_func = 'list';
    var dlt_cont_func = 'delete';
</script>
