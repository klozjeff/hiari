<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Category List </h4>
        </div>
<div class="panel-body" id="demo_s">
    <table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,6" data-show-toggle="true" data-show-columns="false" data-search="true" >
        <thead>
            <tr>
                <th><?php echo translate('title');?></th>
                <th><?php echo translate('date');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>

        <tbody >
        <?php
            $i=0;
            foreach($all_blogs as $row){
                $i++;
        ?>
        <tr>
            <td><?php echo $row['title']; ?></td>
            <td><?php echo $row['date']; ?></td>
            <td class="text-right">
                <span style="font-size:18px;margin-top:10px;color:#00acac !important" class="sidebar-icon ti-pencil" data-toggle="tooltip"
                    onclick="ajax_set_full('edit','<?php echo translate('edit_blog'); ?>','<?php echo translate('successfully_edited!'); ?>','blog_edit','<?php echo $row['blog_id']; ?>');proceed('to_list');"
                     title=" <?php echo translate('edit');?>" data-container="body">

                </span>
                <span style="font-size:18px;margin-top:10px;color:#ff5b57 !important" onclick="delete_confirm('<?php echo $row['blog_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"
                    class="sidebar-icon ti-trash" data-toggle="tooltip" title="<?php echo translate('delete');?>" data-container="body">

                </span>
            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
    </div>
</div>


