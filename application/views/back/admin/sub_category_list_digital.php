<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:59 PM
 */
?>
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Category List </h4>
        </div>

<div class="panel-body" id="demo_s">
    <table id="data-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,3" data-show-toggle="true" data-show-columns="false" data-search="true" >
        <thead>
        <tr>
            <th><?php echo translate('no');?></th>
            <th><?php echo translate('name');?></th>
            <th><?php echo translate('banner');?></th>
            <th><?php echo translate('category');?></th>
            <th class="text-right"><?php echo translate('options');?></th>
        </tr>
        </thead>
        <tbody >
        <?php
        $i=0;
        foreach($all_sub_category as $row){
            $i++;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['sub_category_name']; ?></td>
                <td>
                    <?php
                    if(file_exists('uploads/sub_category_image/'.$row['banner'])){
                        ?>
                        <img class="img-md" src="<?php echo base_url(); ?>uploads/sub_category_image/<?php echo $row['banner']; ?>" height="100px" />
                        <?php
                    } else {
                        ?>
                        <img class="img-md" src="<?php echo base_url(); ?>uploads/sub_category_image/default.jpg" height="100px" />
                        <?php
                    }
                    ?>
                </td>
                <td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></td>
                <td class="text-right">
                    <a  style="font-size:18px;margin-top:10px;color:#00acac !important" class="sidebar_icon ti-pencil" data-toggle="tooltip"
                       onclick="ajax_modal('edit','<?php echo translate('edit_sub-category_(_digital_product_)'); ?>','<?php echo translate('successfully_edited!'); ?>','sub_category_edit_digital','<?php echo $row['sub_category_id']; ?>')"
                        title="<?php echo translate('edit');?>" data-container="body">

                    </a>
                    <a style="font-size:18px;margin-top:10px;color:#ff5b57 !important" onclick="delete_confirm('<?php echo $row['sub_category_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"
                       class="sidebar-icon ti-trash" data-toggle="tooltip"
                       title="<?php echo translate('delete');?>" data-container="body">

                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>

<div id='export-div'>
    <h1 style="display:none;"><?php echo translate('sub_category');?></h1>
    <table id="export-table" data-name='sub_category' data-orientation='p' style="display:none;">
        <thead>
        <tr>
            <th><?php echo translate('no');?></th>
            <th><?php echo translate('name');?></th>
            <th><?php echo translate('category');?></th>
        </tr>
        </thead>

        <tbody >
        <?php
        $i = 0;
        foreach($all_sub_category as $row){
            $i++;
            ?>
            <tr>
                <td><?php echo $i; ?></td>
                <td><?php echo $row['sub_category_name']; ?></td>
                <td><?php echo $this->crud_model->get_type_name_by_id('category',$row['category'],'category_name'); ?></td>
            </tr>
            <?php
        }
        ?>
        </tbody>
    </table>
</div>
    </div>
</div>

