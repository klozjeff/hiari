<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:56 PM
 */
?>

<!-- begin col-10 -->
<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">

            <h4 class="panel-title"> Category List </h4>
        </div>
        <div class="panel-body" id="demo_s">
            <table id="data-table" class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th><?php echo translate('no');?></th>
                    <th><?php echo translate('name');?></th>
                    <th><?php echo translate('banner');?></th>
                    <th class="text-right"><?php echo translate('options');?></th>

                </tr>
                </thead>
                <tbody >
                <?php
                $i = 0;
                foreach($all_categories as $row){
                    $i++;
                    ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['category_name']; ?></td>
                        <td>
                            <?php
                            if(file_exists('uploads/category_image/'.$row['banner'])){
                                ?>
                                <img class="img-md" src="<?php echo base_url(); ?>uploads/category_image/<?php echo $row['banner']; ?>" height="50px" />
                                <?php
                            } else {
                                ?>
                                <img class="img-md" src="<?php echo base_url(); ?>uploads/category_image/default.jpg" height="50px" />
                                <?php
                            }
                            ?>
                        </td>
                        <td class="text-right">

                            <span class="sidebar-icon ti-trash pull-right"
                                  onclick="delete_confirm('<?php echo $row['category_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')" style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                            <span class="sidebar-icon ti-pencil pull-right"   onclick="ajax_modal('edit','<?php echo translate('edit_category_(_digital_product_)'); ?>','<?php echo translate('successfully_edited!'); ?>','category_edit_digital','<?php echo $row['category_id']; ?>')"
                                  style="font-size:18px;margin-top:10px;color:#00acac !important"></span>

                        </td>
                    </tr>
                    <?php
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- end col-10 -->