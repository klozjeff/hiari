<div class="col-md-12">
    <div class="panel panel-inverse">
        <div class="panel-heading">
            <h1 class="panel-title">System Users</h1>
        </div>
<div class="panel-body" id="demo_s">
    <table id="demo-table" class="table table-bordered table-striped"  data-pagination="true" data-show-refresh="true" data-ignorecol="0,2" data-show-toggle="false" data-show-columns="false" data-search="true" >

        <thead>
            <tr>
                <th><?php echo translate('no');?></th>
                <th><?php echo translate('name');?></th>
                <th class="text-right"><?php echo translate('options');?></th>
            </tr>
        </thead>
            
        <tbody >
        <?php
            $i = 0;
            foreach($all_roles as $row){
                $i++;
        ?>
        <tr>
            <td><?php echo $i; ?></td>
            <td><?php echo $row['name']; ?></td>
            <td class="text-right">
                <?php if($row['role_id'] != '1'){ ?>
                <span class="sidebar-icon ti-trash pull-right" onclick="delete_confirm('<?php echo $row['role_id']; ?>','<?php echo translate('really_want_to_delete_this?'); ?>')"style="font-size:18px;margin-top:10px;color:#ff5b57 !important"></span>

                <span class="sidebar-icon ti-pencil pull-right"   onclick="ajax_set_full('edit','<?php echo translate('edit_role'); ?>','<?php echo translate('successfully_edited!'); ?>','role_edit','<?php echo $row['role_id']; ?>')"
                      data-original-title="Edit" data-container="body"
                      style="font-size:18px;margin-top:10px;color:#00acac !important"></span>
                <?php } ?>


            </td>
        </tr>
        <?php
            }
        ?>
        </tbody>
    </table>
</div>
    </div>
</div>
           