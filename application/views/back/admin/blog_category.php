<div id="content" class="content">
    <div class="" style="">
        <!-- begin btn-group -->
        <div class="" >
            <a style="margin-right:3px;"  class="btn btn-primary p-l-40 p-r-40 btn-sm pull-right pro_list_btn"
               href="<?php echo base_url(); ?>index.php/admin/blog/">
                <span class="sidebar-icon ti-view-list-alt"></span> <?php echo translate('back_to_blog_list');?>
            </a>

            <a style="margin-right:3px" class="btn btn-info p-l-40 p-r-40 btn-sm pull-right add_pro_btn"
               onclick="ajax_modal('add','<?php echo translate('add_blog_category'); ?>','<?php echo translate('successfully_added!'); ?>','blog_category_add','')">
                <span class="sidebar-icon ti-write"></span> <?php echo translate('create_blog_category');?>
            </a>



        </div>


        <!-- end btn-toolbar -->
        <!-- begin page-header -->
        <h1 class="page-header"><?php echo translate('manage_blog_categories');?></small></h1>
        <!-- end page-header -->
    </div>
    <!-- begin row -->
    <div class="row" id="list">
        <!-- begin col-2 -->


    </div>


</div>

<script>
	var base_url = '<?php echo base_url(); ?>'
	var user_type = 'admin';
	var module = 'blog_category';
	var list_cont_func = 'list';
	var dlt_cont_func = 'delete';
</script>

