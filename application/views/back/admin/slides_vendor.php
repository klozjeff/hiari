<div id="content" class="content">
    <div class="" style="">
        <h1 class="page-header"><?php echo translate('manager_vendor_slides');?></h1>
    </div>
    <div class="row" id="list">

    </div>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var user_type = 'admin';
    var module = 'slides';
    var list_cont_func = 'vendor_slides';
    var dlt_cont_func = 'delete';
</script>