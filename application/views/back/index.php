<?php
/**
 * Created by PhpStorm.
 * User: Cdr-Ole
 * Date: 3/20/2017
 * Time: 12:41 PM
 */
$system_name	 =  $this->db->get_where('general_settings',array('type' => 'system_name'))->row()->value;
$system_title	 =  $this->db->get_where('general_settings',array('type' => 'system_title'))->row()->value;
include 'includes_top.php';

 ?>
<body>
<div id="page-container" class="page-container fade page-without-sidebar page-header-fixed page-with-top-menu">
    <!--START NAVBAR-->
    <?php include 'header.php'; ?>
    <?php include $this->session->userdata('title').'/top_menu.php';?>
    <!--END NAVBAR-->
        <!--START CONTENT CONTAINER-->
    <div id="fol">
        <div>
            <?php include $this->session->userdata('title').'/'.$page_name.'.php' ?>
        </div>
    </div>
        <!--END CONTENT CONTAINER-->
    <!-- START FOOTER -->
    <?php include 'footer.php'; ?>
    <?php include 'script_texts.php'; ?>
    <!-- END FOOTER -->
</div>
<!-- END OF CONTAINER -->

<!-- SETTINGS - DEMO PURPOSE ONLY -->
<?php include 'includes_bottom.php'; ?>

